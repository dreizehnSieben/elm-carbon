module UI.Icons.Navigation exposing (..)

import Svg exposing (Svg, circle, path)
import Svg.Attributes exposing (cx, cy, d, fill, opacity, r)


add : List (Svg msg)
add =
    [ path
        [ d "M17 15L17 8 15 8 15 15 8 15 8 17 15 17 15 24 17 24 17 17 24 17 24 15z" ]
        []
    ]


addAlt : List (Svg msg)
addAlt =
    [ path
        [ d "M16,4c6.6,0,12,5.4,12,12s-5.4,12-12,12S4,22.6,4,16S9.4,4,16,4 M16,2C8.3,2,2,8.3,2,16s6.3,14,14,14s14-6.3,14-14\tS23.7,2,16,2z" ]
        []
    , path
        [ d "M24 15L17 15 17 8 15 8 15 15 8 15 8 17 15 17 15 24 17 24 17 17 24 17z" ]
        []
    ]


addFilled : List (Svg msg)
addFilled =
    [ path
        [ d "M16,2C8.4,2,2,8.4,2,16s6.4,14,14,14s14-6.4,14-14S23.6,2,16,2z M24,17h-7v7h-2v-7H8v-2h7V8h2v7h7V17z" ]
        []
    , path
        [ fill "none"
        , d "M24 17L17 17 17 24 15 24 15 17 8 17 8 15 15 15 15 8 17 8 17 15 24 15z"
        ]
        []
    ]


apps : List (Svg msg)
apps =
    [ path
        [ d "M8,4v4H4V4H8z M10,2H2v8h8V2z M18,4v4h-4V4H18z M20,2h-8v8h8V2z M28,4v4h-4V4H28z M30,2h-8v8h8V2z M8,14v4H4v-4H8z M10,12H2\tv8h8V12z M18,14v4h-4v-4H18z M20,12h-8v8h8V12z M28,14v4h-4v-4H28z M30,12h-8v8h8V12z M8,24v4H4v-4H8z M10,22H2v8h8V22z M18,24v4h-4\tv-4H18z M20,22h-8v8h8V22z M28,24v4h-4v-4H28z M30,22h-8v8h8V22z" ]
        []
    ]


arrowDown : List (Svg msg)
arrowDown =
    [ path
        [ d "M24.59 16.59L17 24.17 17 2 15 2 15 24.17 7.41 16.59 6 18 16 28 26 18 24.59 16.59z" ]
        []
    ]


arrowDownLeft : List (Svg msg)
arrowDownLeft =
    [ path
        [ d "M22 26L22 24 9.41 24 26 7.41 24.59 6 8 22.59 8 10 6 10 6 26 22 26z" ]
        []
    ]


arrowDownRight : List (Svg msg)
arrowDownRight =
    [ path
        [ d "M10 26L10 24 22.59 24 6 7.41 7.41 6 24 22.59 24 10 26 10 26 26 10 26z" ]
        []
    ]


arrowLeft : List (Svg msg)
arrowLeft =
    [ path
        [ d "M13 26L14.41 24.59 6.83 17 29 17 29 15 6.83 15 14.41 7.41 13 6 3 16 13 26z" ]
        []
    ]


arrowRight : List (Svg msg)
arrowRight =
    [ path
        [ d "M18 6L16.6 7.4 24.1 15 3 15 3 17 24.1 17 16.6 24.6 18 26 28 16z" ]
        []
    ]


arrowUp : List (Svg msg)
arrowUp =
    [ path
        [ d "M16 4L6 14 7.41 15.41 15 7.83 15 30 17 30 17 7.83 24.59 15.41 26 14 16 4z" ]
        []
    ]


arrowUpLeft : List (Svg msg)
arrowUpLeft =
    [ path
        [ d "M22 6L22 8 9.41 8 26 24.59 24.59 26 8 9.41 8 22 6 22 6 6 22 6z" ]
        []
    ]


arrowUpRight : List (Svg msg)
arrowUpRight =
    [ path
        [ d "M10 6L10 8 22.59 8 6 24.59 7.41 26 24 9.41 24 22 26 22 26 6 10 6z" ]
        []
    ]


caretDown : List (Svg msg)
caretDown =
    [ path
        [ d "M24 12L16 22 8 12z" ]
        []
    ]


careLeft : List (Svg msg)
careLeft =
    [ path
        [ d "M20 24L10 16 20 8z" ]
        []
    ]


caretRight : List (Svg msg)
caretRight =
    [ path
        [ d "M12 8L22 16 12 24z" ]
        []
    ]


caretUp : List (Svg msg)
caretUp =
    [ path
        [ d "M8 20L16 10 24 20z" ]
        []
    ]


chevronDown : List (Svg msg)
chevronDown =
    [ path
        [ d "M16 22L6 12 7.4 10.6 16 19.2 24.6 10.6 26 12z" ]
        []
    ]


chevronLeft : List (Svg msg)
chevronLeft =
    [ path
        [ d "M10 16L20 6 21.4 7.4 12.8 16 21.4 24.6 20 26z" ]
        []
    ]


chevronRight : List (Svg msg)
chevronRight =
    [ path
        [ d "M22 16L12 26 10.6 24.6 19.2 16 10.6 7.4 12 6z" ]
        []
    ]


chevronUp : List (Svg msg)
chevronUp =
    [ path
        [ d "M16 10L26 20 24.6 21.4 16 12.8 7.4 21.4 6 20z" ]
        []
    ]


close : List (Svg msg)
close =
    [ path
        [ d "M24 9.4L22.6 8 16 14.6 9.4 8 8 9.4 14.6 16 8 22.6 9.4 24 16 17.4 22.6 24 24 22.6 17.4 16 24 9.4z" ]
        []
    ]


closeFilled : List (Svg msg)
closeFilled =
    [ path
        [ d "M16,2C8.2,2,2,8.2,2,16s6.2,14,14,14s14-6.2,14-14S23.8,2,16,2z M21.4,23L16,17.6L10.6,23L9,21.4l5.4-5.4L9,10.6L10.6,9 l5.4,5.4L21.4,9l1.6,1.6L17.6,16l5.4,5.4L21.4,23z" ]
        []
    , path
        [ opacity "0"
        , d "M14.4 16L9 10.6 10.6 9 16 14.4 21.4 9 23 10.6 17.6 16 23 21.4 21.4 23 16 17.6 10.6 23 9 21.4 14.4 16"
        ]
        []
    ]


closeOutline : List (Svg msg)
closeOutline =
    [ path
        [ d "M16,2C8.2,2,2,8.2,2,16s6.2,14,14,14s14-6.2,14-14S23.8,2,16,2z M16,28C9.4,28,4,22.6,4,16S9.4,4,16,4s12,5.4,12,12\tS22.6,28,16,28z" ]
        []
    , path
        [ d "M21.4 23L16 17.6 10.6 23 9 21.4 14.4 16 9 10.6 10.6 9 16 14.4 21.4 9 23 10.6 17.6 16 23 21.4z" ]
        []
    ]


downToBottom : List (Svg msg)
downToBottom =
    [ path
        [ d "M16 18L6 8 7.4 6.6 16 15.2 24.6 6.6 26 8zM4 22H28V24H4z" ]
        []
    ]


draggable : List (Svg msg)
draggable =
    [ path
        [ d "M10 6H14V10H10zM18 6H22V10H18zM10 14H14V18H10zM18 14H22V18H18zM10 22H14V26H10zM18 22H22V26H18z" ]
        []
    ]


home : List (Svg msg)
home =
    [ path
        [ d "M16.61,2.21a1,1,0,0,0-1.24,0L1,13.42,2.24,15,4,13.62V26a2,2,0,0,0,2,2H26a2,2,0,0,0,2-2V13.63L29.76,15,31,13.43ZM18,26H14V18h4Zm2,0h0V18a2,2,0,0,0-2-2H14a2,2,0,0,0-2,2v8H6V12.06L16,4.27l10,7.8V26Z" ]
        []
    ]


menu : List (Svg msg)
menu =
    [ path
        [ d "M4 24H28V26H4zM4 12H28V14H4zM4 18H28V20H4zM4 6H28V8H4z" ]
        []
    ]


overflowMenuHorizontal : List (Svg msg)
overflowMenuHorizontal =
    [ circle
        [ cx "8", cy "16", r "2" ]
        []
    , circle
        [ cx "16", cy "16", r "2" ]
        []
    , circle
        [ cx "24", cy "16", r "2" ]
        []
    ]


overflowMenuVertical : List (Svg msg)
overflowMenuVertical =
    [ circle
        [ cx "16", cy "8", r "2" ]
        []
    , circle
        [ cx "16", cy "16", r "2" ]
        []
    , circle
        [ cx "16", cy "24", r "2" ]
        []
    ]


pageFirst : List (Svg msg)
pageFirst =
    [ path
        [ d "M14 16L24 6 25.4 7.4 16.8 16 25.4 24.6 24 26zM8 4H10V28H8z" ]
        []
    ]


pageLast : List (Svg msg)
pageLast =
    [ path
        [ d "M18 16L8 26 6.6 24.6 15.2 16 6.6 7.4 8 6zM22 4H24V28H22z" ]
        []
    ]


subtract : List (Svg msg)
subtract =
    [ path
        [ d "M8 15H24V17H8z" ]
        []
    ]


subtractAlt : List (Svg msg)
subtractAlt =
    [ path
        [ d "M16,4c6.6,0,12,5.4,12,12s-5.4,12-12,12S4,22.6,4,16S9.4,4,16,4 M16,2C8.3,2,2,8.3,2,16s6.3,14,14,14s14-6.3,14-14\tS23.7,2,16,2z" ]
        []
    , path
        [ d "M8 15H24V17H8z" ]
        []
    ]


switcher : List (Svg msg)
switcher =
    [ path
        [ d "M14 4H18V8H14zM4 4H8V8H4zM24 4H28V8H24zM14 14H18V18H14zM4 14H8V18H4zM24 14H28V18H24zM14 24H18V28H14zM4 24H8V28H4zM24 24H28V28H24z" ]
        []
    ]


upToTop : List (Svg msg)
upToTop =
    [ path
        [ d "M16 14L6 24 7.4 25.4 16 16.8 24.6 25.4 26 24zM4 8H28V10H4z" ]
        []
    ]


zoomIn : List (Svg msg)
zoomIn =
    [ path
        [ d "M19 13L15 13 15 9 13 9 13 13 9 13 9 15 13 15 13 19 15 19 15 15 19 15 19 13z" ]
        []
    , path
        [ d "M22.45,21A10.87,10.87,0,0,0,25,14,11,11,0,1,0,14,25a10.87,10.87,0,0,0,7-2.55L28.59,30,30,28.59ZM14,23a9,9,0,1,1,9-9A9,9,0,0,1,14,23Z" ]
        []
    ]


zoomOut : List (Svg msg)
zoomOut =
    [ path
        [ d "M9 13H19V15H9z" ]
        []
    , path
        [ d "M22.45,21A10.87,10.87,0,0,0,25,14,11,11,0,1,0,14,25a10.87,10.87,0,0,0,7-2.55L28.59,30,30,28.59ZM14,23a9,9,0,1,1,9-9A9,9,0,0,1,14,23Z" ]
        []
    ]


zoomReset : List (Svg msg)
zoomReset =
    [ path
        [ d "M22.4478,21A10.855,10.855,0,0,0,25,14,10.99,10.99,0,0,0,6,6.4658V2H4v8h8V8H7.332a8.9768,8.9768,0,1,1-2.1,8H3.1912A11.0118,11.0118,0,0,0,14,25a10.855,10.855,0,0,0,7-2.5522L28.5859,30,30,28.5859Z" ]
        []
    ]
