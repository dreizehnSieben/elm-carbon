module UI.Themes.Gray90 exposing (..)

import Element exposing (Color)
import UI.Colors exposing (..)

uiBackground : Color
uiBackground =
    gray90

interactive01 : Color
interactive01 =
    blue60

interactive02 : Color
interactive02 =
    gray60

interactive03 : Color
interactive03 =
    white

interactive04 : Color
interactive04 =
    blue50

danger : Color
danger =
    red60

ui01 : Color
ui01 =
    gray80

ui02 : Color
ui02 =
    gray70

ui03 : Color
ui03 =
    gray70

ui04 : Color
ui04 =
    gray50

ui05 : Color
ui05 =
    gray10

buttonSeparator : Color
buttonSeparator =
    gray100

decorative01 : Color
decorative01 =
    gray60

text01 : Color
text01 =
    gray10

text02 : Color
text02 =
    gray30

text03 : Color
text03 =
    gray60

text04 : Color
text04 =
    white

text05 : Color
text05 =
    gray50

textError : Color
textError =
    red30

link01 : Color
link01 =
    blue40

inverseLink : Color
inverseLink =
    blue60

icon01 : Color
icon01 =
    gray10

icon02 : Color
icon02 =
    gray30

icon03 : Color
icon03 =
    white

field01 : Color
field01 =
    gray80

field02 : Color
field02 =
    gray70

inverse01 : Color
inverse01 =
    gray100

inverse02 : Color
inverse02 =
    gray10

support01 : Color
support01 =
    red40

support02 : Color
support02 =
    green40

support03 : Color
support03 =
    alert30

support04 : Color
support04 =
    blue50

inverseSupport01 : Color
inverseSupport01 =
    red60

inverseSupport02 : Color
inverseSupport02 =
    green50

inverseSupport03 : Color
inverseSupport03 =
    alert30

inverseSupport04 : Color
inverseSupport04 =
    blue70

overlay01 : Color
overlay01 =
    gray100

focus : Color
focus =
    white

inverseFocusUi : Color
inverseFocusUi =
    blue60

hoverPrimary : Color
hoverPrimary =
    blue60hover

hoverPrimaryText : Color
hoverPrimaryText = 
    blue30

hoverSecondary : Color
hoverSecondary =
    gray60hover

hoverTertiary : Color
hoverTertiary =
    gray10

hoverUi : Color
hoverUi =
    gray80hover

hoverLightUi : Color
hoverLightUi =
    gray70hover

hoverSelectedUi : Color
hoverSelectedUi =
    gray70hover

hoverDanger : Color
hoverDanger =
    red60hover

hoverRow : Color
hoverRow =
    gray80hover

inverseHoverUi : Color
inverseHoverUi =
    gray10hover

activePrimary : Color
activePrimary =
    blue80

activeSecondary : Color
activeSecondary =
    gray80

activeTertiary : Color
activeTertiary =
    gray30

activeUi : Color
activeUi =
    gray60

activeLightUi : Color
activeLightUi =
    gray50

activeDanger : Color
activeDanger = 
    red80

selectedUi : Color
selectedUi =
    gray70

selectedLightUi : Color
selectedLightUi =
    gray60

highlight : Color
highlight =
    blue80

skeleton01 : Color
skeleton01 =
    gray80hover

skeleton02 : Color
skeleton02 =
    gray70

visitedLink : Color
visitedLink =
    purple40

disabled01 : Color
disabled01 =
    gray80

disabled02 : Color
disabled02 =
    gray60

disabled03 : Color
disabled03 =
    gray50