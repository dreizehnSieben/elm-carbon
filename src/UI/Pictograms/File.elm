module UI.Pictograms.File exposing (..)

import Svg exposing (Svg, path)
import Svg.Attributes exposing (d, fill, strokeDasharray)


addDocument : List (Svg msg)
addDocument =
    [ path
        [ fill "none"
        , d "M17,20v8 M21,24h-8 M17,16c-4.418,0-8,3.582-8,8s3.582,8,8,8s8-3.582,8-8S21.418,16,17,16z M15,31.75V39h24V15l-6-6H15v7.25 M33,9 v6h6"
        ]
        []
    ]


contract : List (Svg msg)
contract =
    [ path
        [ fill "none"
        , d "M26,11 h5v24H13V11h5 M14,35v2h19V13h-2 M16,37v2h19V15h-2 M24,10V9h-4v1h-2v2h8v-2L24,10z M15.5,17h13 M15.5,21h13 M15.5,25h13 M15.5,29 h13"
        ]
        []
    ]


documentSecurity : List (Svg msg)
documentSecurity =
    [ path
        [ fill "none"
        , d "M24,39H9V9h18l6,6v7 M27,9v6h6 M13,19h16 M13,23l10,0 M13,27h10 M13,31h10 M13,35h10 M26,25v7.5c0,3.575,2.925,6.5,6.5,6.5 s6.5-2.925,6.5-6.5V25H26z"
        ]
        []
    ]


duplicateFile : List (Svg msg)
duplicateFile =
    [ path
        [ fill "none"
        , d "M28,39H9V17h19V39z"
        ]
        []
    , path
        [ fill "none"
        , d "M20 17L20 15M20 11L20 9 22 9"
        ]
        []
    , path
        [ fill "none"
        , strokeDasharray "3.5294,2.6471"
        , d "M24.647 9L35.676 9"
        ]
        []
    , path
        [ fill "none"
        , d "M37 9L39 9 39 11"
        ]
        []
    , path
        [ fill "none"
        , strokeDasharray "4.2353,3.1765"
        , d "M39 14.177L39 27.412"
        ]
        []
    , path
        [ fill "none"
        , d "M39 29L39 31 37 31"
        ]
        []
    , path
        [ fill "none"
        , strokeDasharray "2.8,2.1"
        , d "M34.9 31L31.05 31"
        ]
        []
    , path
        [ fill "none"
        , d "M30 31L28 31"
        ]
        []
    ]


envelope : List (Svg msg)
envelope =
    [ path
        [ fill "none"
        , d "M39 35H9V13h30V35zM9 13l15 14 15-14M9 35L21 24M27 24L39 35"
        ]
        []
    ]


fileBackup : List (Svg msg)
fileBackup =
    [ path
        [ fill "none"
        , d "M24,39H9V17h15V39z M24.015,35h7.5V13h-15v3.985 M31.515,31H39V9H24v4"
        ]
        []
    ]


fileTransfer : List (Svg msg)
fileTransfer =
    [ path
        [ fill "none"
        , d "M39,9v12H9V9 M39,39v-6l-9-9H9v15 M30,24v9h9"
        ]
        []
    ]


folder : List (Svg msg)
folder =
    [ path
        [ fill "none"
        , d "M10,11h8.5c0.315,0,0.611,0.148,0.8,0.4l1.4,2.2c0.189,0.252,0.485,0.4,0.8,0.4H38c0.552,0,1,0.448,1,1l0,19.002 c0,0.552-0.448,1-1,1L10,35c-0.552,0-1-0.448-1-1l0-22C9,11.448,9.447,11,10,11z"
        ]
        []
    ]


richTextFormat : List (Svg msg)
richTextFormat =
    [ path
        [ fill "none"
        , d "M24,31H9V16h15V31z M18.5,18c-0.828,0-1.5,0.672-1.5,1.5s0.672,1.5,1.5,1.5s1.5-0.672,1.5-1.5S19.328,18,18.5,18z M24,28l-4-4l-2,2 l-5-5l-4,4 M15,31v8h24V15l-6-6H15v7 M33,9v6h6 M24,19h11 M24,23h11 M24,27h11 M24,31h11 M19,35h12"
        ]
        []
    ]
