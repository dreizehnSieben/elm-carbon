module UI.Icons.Research exposing (..)

import Svg exposing (Svg, path)
import Svg.Attributes exposing (d)


barrier : List (Svg msg)
barrier =
    [ path
        [ d "M15 5H17V9H15zM15 11H17V15H15zM15 17H17V21H15zM15 23H17V27H15z" ]
        []
    ]


blogSphere : List (Svg msg)
blogSphere =
    [ path
        [ d "M21.87,7.84l-1.74-1L16,14h0a2,2,0,1,0,2,2,2,2,0,0,0-.27-1Z" ]
        []
    , path
        [ d "M16,2A14,14,0,1,0,30,16,14,14,0,0,0,16,2Zm0,2a12,12,0,0,1,11.17,7.65,25.69,25.69,0,0,0-3.69-1.5l-1,1.77a22.7,22.7,0,0,1,5.41,2.39,11.05,11.05,0,0,1,0,3.38A22.92,22.92,0,0,1,16,21,22.92,22.92,0,0,1,4.13,17.69a11.05,11.05,0,0,1,0-3.38A22.8,22.8,0,0,1,15,11V9a25,25,0,0,0-10.17,2.6A12,12,0,0,1,16,4Zm0,24A12,12,0,0,1,4.83,20.35,24.88,24.88,0,0,0,16,23a24.88,24.88,0,0,0,11.17-2.65A12,12,0,0,1,16,28Z" ]
        []
    ]


ccX : List (Svg msg)
ccX =
    [ path
        [ d "M10 23H5a2 2 0 01-2-2V15a2 2 0 012-2h5v2H5v6h5zM19 23H14a2 2 0 01-2-2V15a2 2 0 012-2h5v2H14v6h5zM29 9L27 9 25 15 23 9 21 9 23.75 16 21 23 23 23 25 17 27 23 29 23 26.25 16 29 9z" ]
        []
    ]


circuitComposer : List (Svg msg)
circuitComposer =
    [ path
        [ d "M18 9L18 15 14 15 14 9 12 9 12 23 14 23 14 17 18 17 18 23 20 23 20 9 18 9z" ]
        []
    , path
        [ d "M30,15H26V6a2,2,0,0,0-2-2H8A2,2,0,0,0,6,6v9H2v2H6v9a2,2,0,0,0,2,2H24a2,2,0,0,0,2-2V17h4ZM8,26V6H24V26Z" ]
        []
    ]


composerEdit : List (Svg msg)
composerEdit =
    [ path
        [ d "M25.82,10H30V8H25.82a3,3,0,0,0-5.64,0H13V5H5V8H2v2H5v3h8V10h7.18A3,3,0,0,0,22,11.82v7.32A4,4,0,0,0,19.14,22H2v2H19.14a4,4,0,0,0,7.72,0H30V22H26.86A4,4,0,0,0,24,19.14V11.82A3,3,0,0,0,25.82,10ZM11,11H7V7h4ZM25,23a2,2,0,1,1-2-2A2,2,0,0,1,25,23Z" ]
        []
    ]


cuOne : List (Svg msg)
cuOne =
    [ path
        [ d "M10 23H5a2 2 0 01-2-2V15a2 2 0 012-2h5v2H5v6h5zM18 23H14a2 2 0 01-2-2V9h2V21h4V9h2V21A2 2 0 0118 23zM27 21L27 9.01 22 9.01 22 11.01 25 11.01 25 21 22 21 22 23 30 23 30 21 27 21z" ]
        []
    ]


cuThree : List (Svg msg)
cuThree =
    [ path
        [ d "M10 23H5a2 2 0 01-2-2V15a2 2 0 012-2h5v2H5v6h5zM18 23H14a2 2 0 01-2-2V9h2V21h4V9h2V21A2 2 0 0118 23zM28 9H22v2h6v4H23v2h5v4H22v2h6a2 2 0 002-2V11A2 2 0 0028 9z" ]
        []
    ]


cY : List (Svg msg)
cY =
    [ path
        [ d "M15 23H10a2 2 0 01-2-2V15a2 2 0 012-2h5v2H10v6h5zM24 9L22 9 20 16 18 9 16 9 19 18 19 23 21 23 21 18 24 9z" ]
        []
    ]


cZ : List (Svg msg)
cZ =
    [ path
        [ d "M14 23H9a2 2 0 01-2-2V15a2 2 0 012-2h5v2H9v6h5zM24 9L16 9 16 11 22 11 16 21 16 23 24 23 24 21 18 21 24 11 24 9z" ]
        []
    ]


h : List (Svg msg)
h =
    [ path
        [ d "M18 9L18 15 14 15 14 9 12 9 12 23 14 23 14 17 18 17 18 23 20 23 20 9 18 9z" ]
        []
    ]


hintonPlot : List (Svg msg)
hintonPlot =
    [ path
        [ d "M2 2H6V6H2zM10 2H14V6H10zM18 2H22V6H18zM26 2H30V6H26zM2 10H6V14H2zM10 10H14V14H10zM18 10H22V14H18zM26 10H30V14H26zM2 18H6V22H2zM10 18H14V22H10zM18 18H22V22H18zM26 18H30V22H26zM2 26H6V30H2zM10 26H14V30H10zM18 26H22V30H18zM26 26H30V30H26z" ]
        []
    ]


id : List (Svg msg)
id =
    [ path
        [ d "M10 9H12V11H10zM18 23H14V9h4a4 4 0 014 4v6A4 4 0 0118 23zm-2-2h2a2 2 0 002-2V13a2 2 0 00-2-2H16zM10 13H12V23H10z" ]
        []
    ]


matrix : List (Svg msg)
matrix =
    [ path
        [ d "M18 13L18 4 16 4 16 6 13 6 13 8 16 8 16 13 13 13 13 15 21 15 21 13 18 13zM16.5 20A3.5 3.5 0 1113 23.5 3.5 3.5 0 0116.5 20m0-2A5.5 5.5 0 1022 23.5 5.5 5.5 0 0016.5 18zM8 30L2 30 2 2 8 2 8 4 4 4 4 28 8 28 8 30zM30 30L24 30 24 28 28 28 28 4 24 4 24 2 30 2 30 30z" ]
        []
    ]


operation : List (Svg msg)
operation =
    [ path
        [ d "M23 26L21 26 26 16 21 6 23 6 28 16 23 26zM4 6H6V26H4zM16 9H12a2 2 0 00-2 2V21a2 2 0 002 2h4a2 2 0 002-2V11A2 2 0 0016 9zm0 12H12V11h4z" ]
        []
    , path
        [ d "M13 15H15V17H13z" ]
        []
    ]


operationGauge : List (Svg msg)
operationGauge =
    [ path
        [ d "M30 4L24 4 24 6 27.75 6 24 10 24 12 30 12 30 10 26.38 10 30 6 30 4zM20 17.62L22.08 14l-1.73-1-2.18 3.76A12 12 0 002 28H4a10 10 0 0113.16-9.48L14 24a2 2 0 102 2 2 2 0 00-.27-1L19 19.35A10 10 0 0124 28h2A12 12 0 0020 17.62z" ]
        []
    ]


operationIf : List (Svg msg)
operationIf =
    [ path
        [ d "M12 13H14V23H12zM12 9H14V11H12zM23 11V9H20a2 2 0 00-2 2v2H16v2h2v8h2V15h3V13H20V11z" ]
        []
    ]


s : List (Svg msg)
s =
    [ path
        [ d "M18,23H12V21h6V17H14a2,2,0,0,1-2-2V11a2,2,0,0,1,2-2h6v2H14v4h4a2,2,0,0,1,2,2v4A2,2,0,0,1,18,23Z" ]
        []
    ]


sAlt : List (Svg msg)
sAlt =
    [ path
        [ d "M24 9L22 9 22 7 20 7 20 9 18 9 18 11 20 11 20 17 22 17 22 11 24 11 24 9zM14 23H8V21h6V17H10a2 2 0 01-2-2V11a2 2 0 012-2h6v2H10v4h4a2 2 0 012 2v4A2 2 0 0114 23z" ]
        []
    ]


t : List (Svg msg)
t =
    [ path
        [ d "M12 11L15 11 15 23 17 23 17 11 20 11 20 9 12 9 12 11z" ]
        []
    ]


uOne : List (Svg msg)
uOne =
    [ path
        [ d "M13 23H9a2 2 0 01-2-2V9H9V21h4V9h2V21A2 2 0 0113 23zM22 21L22 9 17 9 17 11 20 11 20 21 17 21 17 23 25 23 25 21 22 21z" ]
        []
    ]


uTwo : List (Svg msg)
uTwo =
    [ path
        [ d "M13 23H9a2 2 0 01-2-2V9H9V21h4V9h2V21A2 2 0 0113 23zM25 23H17V17a2 2 0 012-2h4V11H17V9h6a2 2 0 012 2v4a2 2 0 01-2 2H19v4h6z" ]
        []
    ]


uThree : List (Svg msg)
uThree =
    [ path
        [ d "M13 23H9a2 2 0 01-2-2V9H9V21h4V9h2V21A2 2 0 0113 23zM23 9H17v2h6v4H18v2h5v4H17v2h6a2 2 0 002-2V11A2 2 0 0023 9z" ]
        []
    ]


x : List (Svg msg)
x =
    [ path
        [ d "M20 9L18 9 16 15 14 9 12 9 14.75 16 12 23 14 23 16 17 18 23 20 23 17.25 16 20 9z" ]
        []
    ]


y : List (Svg msg)
y =
    [ path
        [ d "M20 9L18 9 16 16 14 9 12 9 15 18 15 23 17 23 17 18 20 9z" ]
        []
    ]


z : List (Svg msg)
z =
    [ path
        [ d "M20 9L12 9 12 11 18 11 12 21 12 23 20 23 20 21 14 21 20 11 20 9z" ]
        []
    ]
