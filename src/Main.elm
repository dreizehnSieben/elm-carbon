module Main exposing (main)

import Html exposing (Html)
import Element exposing (Element)
import Element.Background as Background
import UI.Components.Button exposing (..)
import UI.Themes.Gray100 exposing (uiBackground)

main : Html msg
main = 
    Element.layout [ Background.color uiBackground ]
        ( renderButton myButton )

myButton : Button msg
myButton =
    Button
        Primary
        FullBleed
        False
        "Default button"
        Nothing