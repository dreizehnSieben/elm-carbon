module UI.Themes.White exposing (..)

import Element exposing (Color)
import UI.Colors exposing (..)

uiBackground : Color
uiBackground =
    white

interactive01 : Color
interactive01 =
    blue60

interactive02 : Color
interactive02 =
    gray80

interactive03 : Color
interactive03 =
    blue60

interactive04 : Color
interactive04 =
    blue60

danger : Color
danger =
    red60

ui01 : Color
ui01 =
    gray10

ui02 : Color
ui02 =
    white

ui03 : Color
ui03 =
    gray20

ui04 : Color
ui04 =
    gray50

ui05 : Color
ui05 =
    gray100

buttonSeparator : Color
buttonSeparator =
    gray20

decorative01 : Color
decorative01 =
    gray20

text01 : Color
text01 =
    gray100

text02 : Color
text02 =
    gray70

text03 : Color
text03 =
    gray40

text04 : Color
text04 =
    white

text05 : Color
text05 =
    gray60

textError : Color
textError = 
    red60

link01 : Color
link01 =
    blue60

inverseLink : Color
inverseLink = 
    blue40

icon01 : Color
icon01 =
    gray100

icon02 : Color
icon02 =
    gray70

icon03 : Color
icon03 =
    white

field01 : Color
field01 =
    gray10

field02 : Color
field02 =
    white

inverse01 : Color
inverse01 =
    white

inverse02 : Color
inverse02 =
    gray80

support01 : Color
support01 =
    red60

support02 : Color
support02 =
    green50

support03 : Color
support03 =
    alert30

support04 : Color
support04 =
    blue70

inverseSupport01 : Color
inverseSupport01 =
    red50

inverseSupport02 : Color
inverseSupport02 =
    green40

inverseSupport03 : Color
inverseSupport03 =
    alert30

inverseSupport04 : Color
inverseSupport04 =
    blue50

overlay01 : Color
overlay01 =
    gray100


focus : Color
focus = 
    blue60

inverseFocusUi : Color
inverseFocusUi =
    white

hoverPrimary : Color
hoverPrimary =
    blue60hover

hoverPrimaryText : Color
hoverPrimaryText =
    blue70

hoverSecondary : Color
hoverSecondary =
    gray80hover

hoverTertiary : Color
hoverTertiary =
    blue60hover

hoverUi : Color
hoverUi =
    gray10hover

hoverLightUi : Color
hoverLightUi =
    gray10hover

hoverSelectedUi : Color
hoverSelectedUi =
    gray20hover

hoverDanger : Color
hoverDanger =
    red60hover

hoverRow : Color
hoverRow = 
    gray10hover

inverseHoverUi : Color
inverseHoverUi =
    gray80hover

activePrimary : Color
activePrimary = 
    blue80

activeSecondary : Color
activeSecondary =
    gray60

activeTertiary : Color
activeTertiary =
    blue80

activeUi : Color
activeUi =
    gray30

activeLightUi : Color
activeLightUi =
    gray30

activeDanger : Color
activeDanger =
    red80

selectedUi : Color
selectedUi =
    gray20

selectedLightUi : Color
selectedLightUi = 
    gray20

highlight : Color
highlight =
    blue20

skeleton01 : Color
skeleton01 =
    gray10hover

skeleton02 : Color
skeleton02 =
    gray30

visitedLink : Color
visitedLink =
    purple60

disabled01 : Color
disabled01 =
    gray10 

disabled02 : Color
disabled02 =
    gray30

disabled03 : Color
disabled03 =
    gray50