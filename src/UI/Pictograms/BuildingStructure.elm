module UI.Pictograms.BuildingStructure exposing (..)

import Svg exposing (Svg, path)
import Svg.Attributes exposing (d, fill)


alarm : List (Svg msg)
alarm =
    [ path
        [ fill "none"
        , d "M16.929,16.929C15.119,18.739,14,21.239,14,24c0,2.761,1.119,5.261,2.929,7.071 M13.393,13.393C10.679,16.108,9,19.858,9,24 c0,4.142,1.679,7.892,4.393,10.607 M20.453,27.547C19.544,26.639,19,25.386,19,24c0-1.385,0.544-2.641,1.451-3.549 M31.071,31.071 C32.881,29.261,34,26.761,34,24c0-2.761-1.119-5.261-2.929-7.071 M34.607,34.607C37.321,31.892,39,28.142,39,24 c0-4.142-1.679-7.892-4.393-10.607 M27.547,20.453C28.456,21.361,29,22.614,29,24c0,1.385-0.544,2.641-1.451,3.549"
        ]
        []
    ]


apartment : List (Svg msg)
apartment =
    [ path
        [ fill "none"
        , d "M22,39h-4v-8h4V39z M22,9v12H10v18h28V9H22z M26,15v-2 M26,21v-2 M14,27v-2 M34,15v-2 M34,21v-2 M22,27v-2 M30,15v-2 M30,21v-2 M34,27v-2 M34,33v-2 M30,27v-2 M30,33v-2 M18,27v-2 M14,33v-2 M26,27v-2 M26,33v-2"
        ]
        []
    ]


building : List (Svg msg)
building =
    [ path
        [ fill "none"
        , d "M9,9 h30 M27,39v-8h-6v8 M11,11v28 M37,11v28 M9,39h30 M14,13v2 M14,19v2 M14,25v2 M19,13v2 M19,19v2 M19,25v2 M24,13v2 M24,19v2 M24,25 v2 M29,13v2 M29,19v2 M29,25v2 M34,13v2 M34,19v2 M34,25v2"
        ]
        []
    ]


elevator : List (Svg msg)
elevator =
    [ path
        [ fill "none"
        , d "M9,9v30 M39,9v30 M23,13l-3-3l-3,3 M25,10l3,3 l3-3"
        ]
        []
    , path
        [ fill "none"
        , d "M25.5,20.5 c0,0.828-0.672,1.5-1.5,1.5c-0.828,0-1.5-0.672-1.5-1.5S23.172,19,24,19C24.829,19,25.5,19.671,25.5,20.5z M34,16H14v23h20V16z M25.39,36.991v-5.352h0l0.346-0.141c0.753-0.306,1.245-1.038,1.246-1.851l0.002-2.261c0.001-1.616-1.208-3.065-2.822-3.15 c-1.721-0.091-3.145,1.278-3.145,2.979l-0.002,2.428c-0.001,0.815,0.494,1.549,1.25,1.855l0.31,0.126l0.036,0.018v5.35"
        ]
        []
    ]


escalatorDown : List (Svg msg)
escalatorDown =
    [ path
        [ fill "none"
        , d "M32,12c1.105,0,2,0.895,2,2s-0.895,2-2,2 c-1.105,0-2-0.895-2-2S30.895,12,32,12z M11,37h6v-6 M17,37l-8-8 M35,26v-5c0-1.657-1.343-3-3-3c-1.657,0-3,1.343-3,3"
        ]
        []
    , path
        [ fill "none"
        , d "M33.44,28.998L18.028,13.586C17.653,13.211,17.144,13,16.614,13H13c-2.209,0-4,1.791-4,4v0.004c0,2.208,1.79,3.997,3.997,3.997 h1.532l15.414,15.413C30.318,36.789,30.827,37,31.357,37h3.647C37.211,37,39,35.211,39,33.004V33c0-2.209-1.791-4.001-4.001-4.001 H33.44"
        ]
        []
    ]


escalatorUp : List (Svg msg)
escalatorUp =
    [ path
        [ fill "none"
        , d "M14.56,28.998l15.412-15.412C30.347,13.211,30.856,13,31.386,13H35c2.209,0,4,1.791,4,4v0.004c0,2.208-1.79,3.997-3.997,3.997 h-1.532L18.057,36.414C17.682,36.789,17.173,37,16.643,37H13c-2.209,0-4-1.791-4-4v0c0-2.209,1.791-4.001,4.001-4.001h1.559"
        ]
        []
    , path
        [ fill "none"
        , d "M18,14c0,1.105-0.895,2-2,2s-2-0.895-2-2 s0.895-2,2-2S18,12.895,18,14z M39,35v-6h-6 M39,29l-8,8 M18.992,21.012C18.992,19.355,17.657,18,16,18s-3,1.343-3,3v5"
        ]
        []
    ]


fireAlarm : List (Svg msg)
fireAlarm =
    [ path
        [ fill "none"
        , d "M32,24c0,4.418-3.582,8-8,8s-8-3.582-8-8s3.582-8,8-8S32,19.582,32,24z M24,22.899c-0.552,0-1,0.448-1,1s0.448,1,1,1s1-0.448,1-1 S24.552,22.899,24,22.899z M14.1,14.101C11.567,16.634,10,20.134,10,24c0,3.866,1.567,7.366,4.101,9.9 M16.222,16.222 C14.231,18.212,13,20.962,13,24c0,3.038,1.231,5.788,3.222,7.778 M33.9,33.899C36.433,31.366,38,27.866,38,24 c0-3.866-1.567-7.366-4.101-9.9 M31.778,31.778C33.769,29.788,35,27.038,35,24c0-3.038-1.231-5.788-3.222-7.778"
        ]
        []
    ]


fireExtinguisher : List (Svg msg)
fireExtinguisher =
    [ path
        [ fill "none"
        , d "M22,17.416v-4.051C22,13.164,22.164,13,22.365,13h3.27C25.836,13,26,13.164,26,13.365v4.051 M25,13V9.365 C25,9.164,24.836,9,24.635,9h-1.27C23.164,9,23,9.164,23,9.365V13 M29,22c0-2.761-2.239-5-5-5h0c-2.761,0-5,2.239-5,5v17h10V22z M23,11h-4l-6,6 M29,10v2l6,1V9L29,10z M29,11h-4 M19,32h10 M19,24h10"
        ]
        []
    ]


idBadge : List (Svg msg)
idBadge =
    [ path
        [ fill "none"
        , d "M18,32c0-2.748,2.092-5.05,4.654-5.657c-1.16-0.516-1.97-1.677-1.97-3.028C20.685,21.484,22.169,20,24,20s3.315,1.484,3.315,3.315 c0,1.351-0.81,2.512-1.97,3.028C27.908,26.95,30,29.252,30,32 M22,14h-8v25h20V14h-8 M26,9h-4v8h4V9z M18,34h12 M18,36h7"
        ]
        []
    ]


office : List (Svg msg)
office =
    [ path
        [ fill "none"
        , d "M20,39 h-4v-8h4V39z M12,15v-2 M12,21v-2 M12,27v-2 M20,15v-2 M20,21v-2 M20,27v-2 M16,15v-2 M16,21v-2 M16,27v-2 M12,33v-2 M24,15v-2 M24,21v-2 M24,27v-2 M24,33v-2 M31,21v-2 M31,27v-2 M31,33v-2 M35,21v-2 M35,27v-2 M35,33v-2 M27,9H9v30h18V9z M39,16H27v23h12V16 z M33,12h-6v4h6V12z"
        ]
        []
    ]


stairsPlanView : List (Svg msg)
stairsPlanView =
    [ path
        [ fill "none"
        , d "M24,10v28 M24,12H10v20h28l0,0 M24,36h14V16H10 M38,20H10 M38,24H10 M38,28H10"
        ]
        []
    ]
