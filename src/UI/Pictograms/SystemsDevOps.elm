module UI.Pictograms.SystemsDevOps exposing (..)

import Svg exposing (Svg, path)
import Svg.Attributes exposing (d, fill)


developerZOS : List (Svg msg)
developerZOS =
    [ path
        [ fill "none"
        , d "M36,22v9l-12,7l-12-7V17 M27.509,22.082L36,17l-12-7l-4,2.325L20,18 M16,14v10 M28,24c0-2.205-1.794-4-4-4c-2.206,0-4,1.795-4,4 c0,2.206,1.794,4,4,4C26.206,28,28,26.206,28,24z"
        ]
        []
    ]


systemsDevOpsAnalyze : List (Svg msg)
systemsDevOpsAnalyze =
    [ path
        [ fill "none"
        , d "M26,27v8 M31,25v10 M36,29v6 M24,38l-12-7V17 M36,23v-6l-12-7l-4,2.325V21 M16,14v10"
        ]
        []
    ]


systemsDevOpsBuild : List (Svg msg)
systemsDevOpsBuild =
    [ path
        [ fill "none"
        , d "M24,38l-12-7 V17 M36,23v-6l-12-7l-4,2.325L20,21 M16,14v10 M38,30.5h-6.5V37H38V30.5z M31.5,24H25v6.5h6.5V24z"
        ]
        []
    ]


systemsDevOpsCICDPipeline : List (Svg msg)
systemsDevOpsCICDPipeline =
    [ path
        [ fill "none"
        , d "M36,22 v9l-12,7l-12-7V17l12-7l12,7l-8.505,5.056 M28,24c0-2.205-1.794-4-4-4c-2.206,0-4,1.795-4,4c0,2.206,1.794,4,4,4 C26.206,28,28,26.206,28,24z"
        ]
        []
    ]


systemsDevOpsCode : List (Svg msg)
systemsDevOpsCode =
    [ path
        [ fill "none"
        , d "M24,38l-12-7V17 M36,23v-6l-12-7 l-4,2.325L19.992,21 M16,14v10"
        ]
        []
    , path
        [ fill "none"
        , d "M34 34L38 30 34 26M29 26L25 30 29 34"
        ]
        []
    ]


systemsDevOpsDeploy : List (Svg msg)
systemsDevOpsDeploy =
    [ path
        [ fill "none"
        , d "M34 27L38 31 34 35"
        ]
        []
    , path
        [ fill "none"
        , d "M12,17v14l12,7 M20,21 v-8.675L24,10l12,7v6 M16,14v10 M28.033,26H26c-1.381,0-2.5,1.119-2.5,2.5S24.619,31,26,31h12"
        ]
        []
    ]


systemsDevOpsNonitor : List (Svg msg)
systemsDevOpsNonitor =
    [ path
        [ fill "none"
        , d "M38,28h-3l-2,7l-3-8l-2,5.007h-3 M24,38l-12-7V17 M36,23v-6l-12-7l-4,2.325V21 M16,14v10"
        ]
        []
    ]


systemsDevOpsPlan : List (Svg msg)
systemsDevOpsPlan =
    [ path
        [ fill "none"
        , d "M33.615,33.615L31.25,31.25V27 M24,38l-12-7V17 M36,23v-6l-12-7l-4,2.325V21 M16,14v10 M38,31.25c0,3.728-3.022,6.75-6.75,6.75 s-6.75-3.022-6.75-6.75s3.022-6.75,6.75-6.75S38,27.522,38,31.25z"
        ]
        []
    ]


systemsDevOpsProvision : List (Svg msg)
systemsDevOpsProvision =
    [ path
        [ fill "none"
        , d "M24,38 l-12-7V17 M36,23v-6l-12-7l-4,2.325V21 M16,14v10 M35.5,33c-1.381,0-2.5,1.119-2.5,2.5c0,1.381,1.119,2.5,2.5,2.5 s2.5-1.119,2.5-2.5C38,34.119,36.881,33,35.5,33z M30.828,30.828l2.904,2.904 M28,24c-2.209,0-4,1.791-4,4s1.791,4,4,4s4-1.791,4-4 S30.209,24,28,24z"
        ]
        []
    ]


systemsDevOpsRelease : List (Svg msg)
systemsDevOpsRelease =
    [ path
        [ fill "none"
        , d "M27.438,28l4.062-4l4.062,4 M31.5,24v9"
        ]
        []
    , path
        [ fill "none"
        , d "M36,31v5h-9v-5 M24,38 l-12-7V17 M36,22v-5l-12-7l-4,2.325V21 M16,14v10"
        ]
        []
    ]


systemsDevOpsTest : List (Svg msg)
systemsDevOpsTest =
    [ path
        [ fill "none"
        , d "M35.257,29.742L31,34l-2.555-2.555 M12,17v14l12,7 M20,21v-8.675L24,10l12,7v6 M16,14v10 M31.25,24.5 c-3.728,0-6.75,3.022-6.75,6.75S27.522,38,31.25,38S38,34.978,38,31.25S34.978,24.5,31.25,24.5z"
        ]
        []
    ]
