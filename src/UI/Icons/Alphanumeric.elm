module UI.Icons.Alphanumeric exposing (..)

import Svg exposing (Svg, path)
import Svg.Attributes exposing (d)


letterAa : List (Svg msg)
letterAa =
    [ path
        [ d "M23 13H18v2h5v2H19a2 2 0 00-2 2v2a2 2 0 002 2h6V15A2 2 0 0023 13zm0 8H19V19h4zM13 9H9a2 2 0 00-2 2V23H9V18h4v5h2V11A2 2 0 0013 9zM9 16V11h4v5z" ]
        []
    ]


letterBb : List (Svg msg)
letterBb =
    [ path
        [ d "M23 13H19V9H17V23h6a2 2 0 002-2V15A2 2 0 0023 13zm-4 8V15h4v6zM15 12a3 3 0 00-3-3H7V23h5a3 3 0 003-3V18a3 3 0 00-.78-2A3 3 0 0015 14zM9 11h3a1 1 0 011 1v2a1 1 0 01-1 1H9zm4 9a1 1 0 01-1 1H9V17h3a1 1 0 011 1z" ]
        []
    ]


letterCc : List (Svg msg)
letterCc =
    [ path
        [ d "M24 23H19a2 2 0 01-2-2V15a2 2 0 012-2h5v2H19v6h5zM15 23H9a2 2 0 01-2-2V11A2 2 0 019 9h6v2H9V21h6z" ]
        []
    ]


letterDd : List (Svg msg)
letterDd =
    [ path
        [ d "M23 9v4H19a2 2 0 00-2 2v6a2 2 0 002 2h6V9zm-4 6h4v6H19zM11 23H7V9h4a4 4 0 014 4v6A4 4 0 0111 23zM9 21h2a2 2 0 002-2V13a2 2 0 00-2-2H9z" ]
        []
    ]


letterEe : List (Svg msg)
letterEe =
    [ path
        [ d "M15 11L15 9.02 7 9.02 7 23 15 23 15 21 9 21 9 17 14 17 14 15 9 15 9 11 15 11zM25 19V15a2 2 0 00-2-2H19a2 2 0 00-2 2v6a2 2 0 002 2h5V21H19V19zm-6-4h4v2H19z" ]
        []
    ]


letterFf : List (Svg msg)
letterFf =
    [ path
        [ d "M16 11L16 9 8 9 8 23 10 23 10 17 15 17 15 15 10 15 10 11 16 11zM24 11V9H21a2 2 0 00-2 2v2H17v2h2v8h2V15h3V13H21V11z" ]
        []
    ]


letterGg : List (Svg msg)
letterGg =
    [ path
        [ d "M19 13a2 2 0 00-2 2v6a2 2 0 002 2h4v2H18v2h5a2 2 0 002-2V13zm4 8H19V15h4zM15 23H9a2 2 0 01-2-2V11A2 2 0 019 9h6v2H9V21h4V17H11V15h4z" ]
        []
    ]


letterHh : List (Svg msg)
letterHh =
    [ path
        [ d "M13 9L13 15 9 15 9 9 7 9 7 23 9 23 9 17 13 17 13 23 15 23 15 9 13 9zM23 13H19V9H17V23h2V15h4v8h2V15A2 2 0 0023 13z" ]
        []
    ]


letterIi : List (Svg msg)
letterIi =
    [ path
        [ d "M10 11L13 11 13 21 10 21 10 23 18 23 18 21 15 21 15 11 18 11 18 9 10 9 10 11zM20 13H22V23H20zM20 9H22V11H20z" ]
        []
    ]


letterJj : List (Svg msg)
letterJj =
    [ path
        [ d "M20 9H22V11H20zM20 25H17v2h3a2 2 0 002-2V13H20zM14 23H10a2 2 0 01-2-2V19h2v2h4V9h2V21A2 2 0 0114 23z" ]
        []
    ]


letterKk : List (Svg msg)
letterKk =
    [ path
        [ d "M15 9L12.89 9 9 15.55 9 9 7 9 7 23 9 23 9 18.71 9.93 17.22 12.89 23 15 23 11.11 15.43 15 9zM22.78 23L25 23 21.22 17 25 13 22.76 13 19 17.17 19 9 17 9 17 23 19 23 19 19.25 19.96 18.21 22.78 23z" ]
        []
    ]


letterLl : List (Svg msg)
letterLl =
    [ path
        [ d "M11 21L11 9 9 9 9 23 17 23 17 21 11 21zM23 23H21a2 2 0 01-2-2V9h2V21h2z" ]
        []
    ]


letterMm : List (Svg msg)
letterMm =
    [ path
        [ d "M24 13H16V23h2V15h2v8h2V15h2v8h2V15A2 2 0 0024 13zM12 9L10.48 14 10 15.98 9.54 14 8 9 6 9 6 23 8 23 8 15 7.84 13 8.42 15 10 19.63 11.58 15 12.16 13 12 15 12 23 14 23 14 9 12 9z" ]
        []
    ]


letterNn : List (Svg msg)
letterNn =
    [ path
        [ d "M25 23H23V15H19v8H17V13h6a2 2 0 012 2zM13 19L9.32 9 7 9 7 23 9 23 9 13 12.68 23 15 23 15 9 13 9 13 19z" ]
        []
    ]


letterOo : List (Svg msg)
letterOo =
    [ path
        [ d "M23 23H19a2 2 0 01-2-2V15a2 2 0 012-2h4a2 2 0 012 2v6A2 2 0 0123 23zm-4-8v6h4V15zM13 23H9a2 2 0 01-2-2V11A2 2 0 019 9h4a2 2 0 012 2V21A2 2 0 0113 23zM9 11V21h4V11z" ]
        []
    ]


letterPp : List (Svg msg)
letterPp =
    [ path
        [ d "M23 13H17V27h2V23h4a2 2 0 002-2V15A2 2 0 0023 13zm-4 8V15h4v6zM9 23H7V9h6a2 2 0 012 2v5a2 2 0 01-2 2H9zm0-7h4V11H9z" ]
        []
    ]


letterQq : List (Svg msg)
letterQq =
    [ path
        [ d "M19 13a2 2 0 00-2 2v6a2 2 0 002 2h4v4h2V13zm4 8H19V15h4zM13 9H9a2 2 0 00-2 2V21a2 2 0 002 2h1v2a2 2 0 002 2h2V25H12V23h1a2 2 0 002-2V11A2 2 0 0013 9zM9 21V11h4V21z" ]
        []
    ]


letterRr : List (Svg msg)
letterRr =
    [ path
        [ d "M16 15V11a2 2 0 00-2-2H8V23h2V17h1.48l2.34 6H16l-2.33-6H14A2 2 0 0016 15zm-6-4h4v4H10zM24 13L18 13 18 23 20 23 20 15 24 15 24 13z" ]
        []
    ]


letterSs : List (Svg msg)
letterSs =
    [ path
        [ d "M22 23H17V21h5V19H19a2 2 0 01-2-2V15a2 2 0 012-2h5v2H19v2h3a2 2 0 012 2v2A2 2 0 0122 23zM13 23H7V21h6V17H9a2 2 0 01-2-2V11A2 2 0 019 9h6v2H9v4h4a2 2 0 012 2v4A2 2 0 0113 23z" ]
        []
    ]


letterTt : List (Svg msg)
letterTt =
    [ path
        [ d "M8 11L11 11 11 23 13 23 13 11 16 11 16 9 8 9 8 11zM23 15V13H20V11H18v2H16v2h2v6a2 2 0 002 2h3V21H20V15z" ]
        []
    ]


letterUu : List (Svg msg)
letterUu =
    [ path
        [ d "M23 23H19a2 2 0 01-2-2V13h2v8h4V13h2v8A2 2 0 0123 23zM13 23H9a2 2 0 01-2-2V9H9V21h4V9h2V21A2 2 0 0113 23z" ]
        []
    ]


letterVv : List (Svg msg)
letterVv =
    [ path
        [ d "M25 13L23.25 13 21 22.03 18.79 13 17 13 19.5 23 22.5 23 25 13zM13 9L11 22 9 9 7 9 9.52 23 12.48 23 15 9 13 9z" ]
        []
    ]


letterWw : List (Svg msg)
letterWw =
    [ path
        [ d "M24.3 13L23.39 21.61 22 13 20 13 18.61 21.61 17.7 13 16 13 17.36 23 19.64 23 21 14.63 22.36 23 24.64 23 26 13 24.3 13zM12.21 9L11.87 17 11.61 21.54 11.2 18 10.52 12.54 8.5 12.54 7.82 18 7.41 21.54 7.16 17 6.81 9 5.01 9 6.01 23 8.28 23 9.04 18.07 9.5 14 9.51 13.97 9.52 14 9.98 18.07 10.74 23 13.01 23 14.01 9 12.21 9z" ]
        []
    ]


letterXx : List (Svg msg)
letterXx =
    [ path
        [ d "M15 9L13 9 11 15 9 9 7 9 9.75 16 7 23 9 23 11 17 13 23 15 23 12.24 16 15 9zM25 13L23 13 21 16.9 19 13 17 13 19.91 18 17 23 19 23 21 19.2 23 23 25 23 22.1 18 25 13z" ]
        []
    ]


letterYy : List (Svg msg)
letterYy =
    [ path
        [ d "M15 9L13 9 11 16 9 9 7 9 10 18 10 23 12 23 12 18 12 18 15 9zM23 13l-2 7.52L19.08 13H17l3.15 9.87L19.53 25H17v2h2.26a2 2 0 001.91-1.42L25 13z" ]
        []
    ]


letterZz : List (Svg msg)
letterZz =
    [ path
        [ d "M25 15L25 13 17 13 17 15 22.5 15 17 21 17 23 25 23 25 21 19.51 21 25 15zM15 9L7 9 7 11 13 11 7 21 7 23 15 23 15 21 9 21 15 11 15 9z" ]
        []
    ]


numberZero : List (Svg msg)
numberZero =
    [ path
        [ d "M18,23H14a2,2,0,0,1-2-2V11a2,2,0,0,1,2-2h4a2,2,0,0,1,2,2V21A2,2,0,0,1,18,23ZM14,11h0V21h4V11Z" ]
        []
    , path
        [ d "M15 15H17V17H15z" ]
        []
    ]


numberOne : List (Svg msg)
numberOne =
    [ path
        [ d "M16,10V22h0V10m1-1H12v2h3V21H12v2h8V21H17V9Z" ]
        []
    ]


numberTwo : List (Svg msg)
numberTwo =
    [ path
        [ d "M20,23H12V17a2,2,0,0,1,2-2h4V11H12V9h6a2,2,0,0,1,2,2v4a2,2,0,0,1-2,2H14v4h6Z" ]
        []
    ]


numberThree : List (Svg msg)
numberThree =
    [ path
        [ d "M18,9H12v2h6v4H14v2h4v4H12v2h6a2,2,0,0,0,2-2V11A2,2,0,0,0,18,9Z" ]
        []
    ]


numberFour : List (Svg msg)
numberFour =
    [ path
        [ d "M18,10v8h0V10m1-1H17v8H14V9H12V19h5v4h2V19h1V17H19V9Z" ]
        []
    ]


numberFive : List (Svg msg)
numberFive =
    [ path
        [ d "M18,23H12V21h6V17H12V9h8v2H14v4h4a2,2,0,0,1,2,2v4A2,2,0,0,1,18,23Z" ]
        []
    ]


numberSix : List (Svg msg)
numberSix =
    [ path
        [ d "M18,14H14V11h5V9H14a2,2,0,0,0-2,2V21a2,2,0,0,0,2,2h4a2,2,0,0,0,2-2V16A2,2,0,0,0,18,14Zm-4,7V16h4v5Z" ]
        []
    ]


numberSeven : List (Svg msg)
numberSeven =
    [ path
        [ d "M20 9L12 9 12 13 14 13 14 11 17.85 11 13 23 15.16 23 20 11 20 9z" ]
        []
    ]


numberEight : List (Svg msg)
numberEight =
    [ path
        [ d "M18,9H14a2,2,0,0,0-2,2V21a2,2,0,0,0,2,2h4a2,2,0,0,0,2-2V11A2,2,0,0,0,18,9Zm0,2v4H14V11ZM14,21V17h4v4Z" ]
        []
    ]


numberNine : List (Svg msg)
numberNine =
    [ path
        [ d "M18,9H14a2,2,0,0,0-2,2v5a2,2,0,0,0,2,2h4v3H13v2h5a2,2,0,0,0,2-2V11A2,2,0,0,0,18,9Zm0,7H14V11h4Z" ]
        []
    ]


numberSmallZero : List (Svg msg)
numberSmallZero =
    [ path
        [ d "M17,21H15a2,2,0,0,1-2-2V13a2,2,0,0,1,2-2h2a2,2,0,0,1,2,2v6A2,2,0,0,1,17,21Zm-2-8v6h2V13Z" ]
        []
    ]


numberSmallOne : List (Svg msg)
numberSmallOne =
    [ path
        [ d "M17 19L17 11 15 11 15 12 13 12 13 14 15 14 15 19 13 19 13 21 19 21 19 19 17 19z" ]
        []
    ]


numberSmallTwo : List (Svg msg)
numberSmallTwo =
    [ path
        [ d "M19,21H13V17a2,2,0,0,1,2-2h2V13H13V11h4a2,2,0,0,1,2,2v2a2,2,0,0,1-2,2H15v2h4Z" ]
        []
    ]


numberSmallThree : List (Svg msg)
numberSmallThree =
    [ path
        [ d "M17,11H13v2h4v2H14v2h3v2H13v2h4a2,2,0,0,0,2-2V13A2,2,0,0,0,17,11Z" ]
        []
    ]


numberSmallFour : List (Svg msg)
numberSmallFour =
    [ path
        [ d "M17 11L17 15 15 15 15 11 13 11 13 17 17 17 17 21 19 21 19 11 17 11z" ]
        []
    ]


numberSmallFive : List (Svg msg)
numberSmallFive =
    [ path
        [ d "M17,21H13V19h4V17H13V11h6v2H15v2h2a2,2,0,0,1,2,2v2A2,2,0,0,1,17,21Z" ]
        []
    ]


numberSmallSix : List (Svg msg)
numberSmallSix =
    [ path
        [ d "M17,21H15a2,2,0,0,1-2-2V13a2,2,0,0,1,2-2h3v2H15v2h2a2,2,0,0,1,2,2v2A2,2,0,0,1,17,21Zm-2-4v2h2V17Z" ]
        []
    ]


numberSmallSeven : List (Svg msg)
numberSmallSeven =
    [ path
        [ d "M16.44 21L14.44 21 17 13 15 13 15 14 13 14 13 11 19 11 19 13 16.44 21z" ]
        []
    ]


numberSmallEight : List (Svg msg)
numberSmallEight =
    [ path
        [ d "M17,11H15a2,2,0,0,0-2,2v6a2,2,0,0,0,2,2h2a2,2,0,0,0,2-2V13A2,2,0,0,0,17,11Zm0,2v2H15V13Zm-2,6V17h2v2Z" ]
        []
    ]


numberSmallNine : List (Svg msg)
numberSmallNine =
    [ path
        [ d "M17,21H14V19h3V17H15a2,2,0,0,1-2-2V13a2,2,0,0,1,2-2h2a2,2,0,0,1,2,2v6A2,2,0,0,1,17,21Zm-2-8v2h2V13Z" ]
        []
    ]


omega : List (Svg msg)
omega =
    [ path
        [ d "M22.7373,25A14.3093,14.3093,0,0,0,27,15C27,8.42,22.58,4,16,4S5,8.42,5,15A14.3093,14.3093,0,0,0,9.2627,25H4v2h9V25.4722l-.4355-.2979A12.646,12.646,0,0,1,7,15c0-5.4673,3.5327-9,9-9s9,3.5327,9,9a12.5671,12.5671,0,0,1-5,9.7615V27h8V25Z" ]
        []
    ]


sigma : List (Svg msg)
sigma =
    [ path
        [ d "M24 5L7 5 7 7.414 15.586 16 7 24.586 7 27 24 27 24 25 9.414 25 18.414 16 9.414 7 24 7 24 5z" ]
        []
    ]
