module UI.Components.Button exposing (Button, Kind(..), Size(..), renderButton)

import Element exposing (Element, Length, px)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import UI.Themes.Gray100 exposing (activePrimary, focus, hoverPrimary, interactive01, text04, disabled02, disabled03)
import UI.Typography exposing (bodyShort01)


type alias Button msg =
    { kind : Kind
    , size : Size
    , disabled : Bool
    , label : String
    , onPress : Maybe msg
    }


type Kind
    = Primary
    | Secondary
    | Tertiary
    | Danger
    | Ghost


type Size
    = FullBleed
    | Default
    | Field
    | Small


renderButton : Button msg -> Element msg
renderButton model =
    case model.kind of
        Primary ->
            renderPrimary model

        _ ->
            renderPrimary model


renderPrimary : Button msg -> Element msg
renderPrimary model =
    bodyShort01 
        (if model.disabled then
            Input.button
                [ Element.height <| getSize model.size
                , Background.color disabled02
                , Font.color disabled03
                , Border.width 1
                , Border.color disabled02
                , Element.focused
                    [ Border.color disabled02 ]
                , Element.paddingEach
                    { top = 0
                    , right = 64
                    , bottom = 0
                    , left = 16
                    }
                ]
                { onPress = Nothing
                , label = Element.text model.label
                }

         else
            Input.button
                [ Element.height <| getSize model.size
                , Background.color interactive01
                , Font.color text04
                , Border.width 1
                , Border.color interactive01
                , Element.mouseOver
                    [ Background.color hoverPrimary ]
                , Element.mouseDown
                    [ Background.color activePrimary ]
                , Element.focused
                    [ Border.color focus
                    ]
                , Element.paddingEach
                    { top = 0
                    , right = 64
                    , bottom = 0
                    , left = 16
                    }
                ]
                { onPress = model.onPress
                , label = Element.text model.label
                }
        )


getSize : Size -> Length
getSize size =
    case size of
        FullBleed ->
            px 64

        Default ->
            px 48

        Field ->
            px 40

        Small ->
            px 32
