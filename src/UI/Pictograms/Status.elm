module UI.Pictograms.Status exposing (..)

import Svg exposing (Svg, circle, path)
import Svg.Attributes exposing (cx, cy, d, fill, r)


question : List (Svg msg)
question =
    [ path
        [ fill "none"
        , d "M17.33,20.712 c0-3.706,2.991-6.71,6.68-6.71c3.689,0,6.68,2.363,6.68,6.069c0,6.676-6.695,4.732-6.695,11.924 M31.488,37 C35.978,34.408,39,29.557,39,24c0-8.284-6.716-15-15-15S9,15.716,9,24c0,5.557,3.022,10.408,7.512,13"
        ]
        []
    , circle
        [ cx "24"
        , cy "37.5"
        , r "1"
        ]
        []
    ]


warningOne : List (Svg msg)
warningOne =
    [ path
        [ fill "none"
        , d "M39,37H9l15-26L39,37z M24,28V18"
        ]
        []
    , circle
        [ cx "24"
        , cy "32"
        , r "1"
        ]
        []
    ]


warningTwo : List (Svg msg)
warningTwo =
    [ path
        [ fill "none"
        , d "M16.511,37 C12.021,34.408,9,29.557,9,24c0-8.284,6.716-15,15-15s15,6.716,15,15c0,5.557-3.022,10.408-7.512,13 M24,32V14"
        ]
        []
    , circle
        [ cx "24"
        , cy "37.5"
        , r "1"
        ]
        []
    ]
