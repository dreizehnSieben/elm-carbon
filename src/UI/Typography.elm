module UI.Typography exposing (..)

import Element exposing (Attr, Attribute, Element)
import Element.Font as Font


sans : Attribute msg
sans =
    Font.family [ Font.typeface "IBM Plex Sans" ]


serif : Attribute msg
serif =
    Font.family [ Font.typeface "IBM Plex Serif" ]


mono : Attribute msg
mono =
    Font.family [ Font.typeface "IBM Plex Mono" ]


condensed : Attribute msg
condensed =
    Font.family [ Font.typeface "IBM Plex Condensed" ]


plex12 : Attr decorative msg
plex12 =
    Font.size 12


plex14 : Attr decorative msg
plex14 =
    Font.size 14


plex16 : Attr decorative msg
plex16 =
    Font.size 16


plex18 : Attr decorative msg
plex18 =
    Font.size 18


plex20 : Attr decorative msg
plex20 =
    Font.size 20


plex24 : Attr decorative msg
plex24 =
    Font.size 24


plex28 : Attr decorative msg
plex28 =
    Font.size 28


plex32 : Attr decorative msg
plex32 =
    Font.size 32


plex36 : Attr decorative msg
plex36 =
    Font.size 36


plex42 : Attr decorative msg
plex42 =
    Font.size 42


plex48 : Attr decorative msg
plex48 =
    Font.size 48


plex54 : Attr decorative msg
plex54 =
    Font.size 54


plex60 : Attr decorative msg
plex60 =
    Font.size 60


plex68 : Attr decorative msg
plex68 =
    Font.size 68


plex76 : Attr decorative msg
plex76 =
    Font.size 76


plex84 : Attr decorative msg
plex84 =
    Font.size 84


plex92 : Attr decorative msg
plex92 =
    Font.size 92

plex102 : Attr decorative msg
plex102 =
    Font.size 102


plex112 : Attr decorative msg
plex112 = 
    Font.size 112


plex122 : Attr decorative msg
plex122 =
    Font.size 122


plex132 : Attr decorative msg
plex132 =
    Font.size 132


plex144 : Attr decorative msg
plex144 =
    Font.size 144


plex156 : Attr decorative msg
plex156 =
    Font.size 156

semiBold : Attribute msg
semiBold =
    Font.semiBold


regular : Attribute msg
regular =
    Font.regular


light : Attribute msg
light =
    Font.light


italic : Attribute msg
italic =
    Font.italic


code01 : Element msg -> Element msg
code01 =
    Element.el
        [ mono
        , plex12
        , regular
        , Element.spacing 4
        ]


code02 : Element msg -> Element msg
code02 =
    Element.el
        [ mono
        , plex14
        , regular
        , Element.spacing 6
        ]


caption01 : Element msg -> Element msg
caption01 =
    Element.el
        [ sans
        , plex12
        , regular
        , Element.spacing 4
        ]


helperText01 : Element msg -> Element msg
helperText01 =
    Element.el
        [ sans
        , plex12
        , regular
        , italic
        , Element.spacing 4
        ]


bodyShort01 : Element msg -> Element msg
bodyShort01 =
    Element.el
        [ sans
        , plex14
        , regular
        , Element.spacing 4
        ]


bodyLong01 : Element msg -> Element msg
bodyLong01 =
    Element.el
        [ sans
        , plex14
        , regular
        , Element.spacing 6
        ]


bodyShort02 : Element msg -> Element msg
bodyShort02 =
    Element.el
        [ sans
        , plex16
        , regular
        , Element.spacing 6
        ]


bodyLong02 : Element msg -> Element msg
bodyLong02 =
    Element.el
        [ sans
        , plex16
        , regular
        , Element.spacing 8
        ]


heading01 : Element msg -> Element msg
heading01 =
    Element.el
        [ sans
        , plex14
        , semiBold
        , Element.spacing 4
        ]


heading02 : Element msg -> Element msg
heading02 =
    Element.el
        [ sans
        , plex16
        , semiBold
        , Element.spacing 6
        ]


expressiveHeading04 : Element msg -> Element msg
expressiveHeading04 =
    Element.el
        [ sans
        , plex32
        , regular
        , Element.spacing 8
        ]


expressiveHeading05 : Element msg -> Element msg
expressiveHeading05 =
    Element.el
        [ sans
        , plex60
        , light
        , Element.spacing 10
        ]


expressiveParagraph01 : Element msg -> Element msg
expressiveParagraph01 =
    Element.el
        [ sans
        , plex32
        , light
        , Element.spacing 8
        ]


quotation01 : List (Element msg) -> Element msg
quotation01 children =
    Element.paragraph
        [ serif
        , plex32
        , light
        , Element.spacing 8
        ]
    <|
        [ Element.text "\"" ]
            ++ children
            ++ [ Element.text "\"" ]


quotation02 : List (Element msg) -> Element msg
quotation02 children =
    Element.paragraph
        [ serif
        , plex60
        , light
        , Element.spacing 10
        ]
    <|
        [ Element.text "\"" ]
            ++ children
            ++ [ Element.text "\"" ]


display01 : List (Element msg) -> Element msg
display01 =
    Element.paragraph
        [ sans
        , plex76
        , light
        , Element.spacing 10
        ]


display02 : List (Element msg) -> Element msg
display02 =
    Element.paragraph
        [ sans
        , plex76
        , semiBold
        , Element.spacing 10
        ]


display03 : List (Element msg) -> Element msg
display03 =
    Element.paragraph
        [ sans
        , plex156
        , light
        , Element.spacing 8
        ]


display04 : List (Element msg) -> Element msg
display04 =
    Element.paragraph
        [ sans
        , plex156
        , semiBold
        , Element.spacing 8
        ]