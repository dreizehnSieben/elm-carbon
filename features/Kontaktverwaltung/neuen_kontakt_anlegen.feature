#language: de
Funktionalität: Neuen Kontakt anlegen
    Als Anwender möchte ich einen neuen Kontakt anlegen können.

    Als Anwender möchte ich die Möglichkeit haben, die Eingabe
    abbrechen zu können.

    Grundlage:
        Angenommen der Benutzer befindet sich in der Anwendung

    Beispiel: Benutzer legt einen Personenkontakt an
        Angenommen der Benutzer sieht das Formular zum Anlegen einer Person
        Wenn die Pflichtfelder ausgefüllt wurden
        Und die Eingaben vom Benutzer gespeichert werden
        Dann sieht der Benutzer diese Person in der Kontaktübersicht