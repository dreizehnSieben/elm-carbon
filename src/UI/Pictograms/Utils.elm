module UI.Pictograms.Utils exposing
    ( pictogram112
    , pictogram48
    , pictogram64
    , pictogram80
    , pictogram96
    )

import Element exposing (Element, html)
import Svg exposing (Attribute, Svg, svg)
import Svg.Attributes exposing (height, stroke, strokeLinecap, strokeLinejoin, strokeMiterlimit, strokeWidth, viewBox, width, x, y)


pictogram : Int -> List (Svg msg) -> Element msg
pictogram size data =
    html <|
        svg
            [ x "0"
            , y "0"
            , width <| String.fromInt size
            , height <| String.fromInt size
            , viewBox "0 0 48 48"
            , stroke "currentColor"
            , strokeLinecap "round"
            , strokeLinejoin "round"
            , strokeMiterlimit "10"
            , strokeWidth ".72"
            ]
            data


pictogram48 : List (Svg msg) -> Element msg
pictogram48 =
    pictogram 48


pictogram64 : List (Svg msg) -> Element msg
pictogram64 =
    pictogram 64


pictogram80 : List (Svg msg) -> Element msg
pictogram80 =
    pictogram 80


pictogram96 : List (Svg msg) -> Element msg
pictogram96 =
    pictogram 96


pictogram112 : List (Svg msg) -> Element msg
pictogram112 =
    pictogram 112
