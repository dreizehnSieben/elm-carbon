module UI.Pictograms.Security exposing (..)

import Svg exposing (Svg, circle, path)
import Svg.Attributes exposing (cx, cy, d, fill, r)


firewall : List (Svg msg)
firewall =
    [ path
        [ fill "none"
        , d "M11.828,13.172L9,16l2.828,2.828 M36.172,34.828L39,32l-2.828-2.828 M9,16h7.616 M25,16h14 M39,32H25 M16.616,32H9"
        ]
        []
    , path
        [ fill "none"
        , d "M19 9H31V39H19z"
        ]
        []
    ]


lockOne : List (Svg msg)
lockOne =
    [ circle
        [ cx "24"
        , cy "28.5"
        , r "1.5"
        ]
        []
    , path
        [ fill "none"
        , d "M34,29c0,5.523-4.477,10-10,10s-10-4.477-10-10s4.477-10,10-10S34,23.477,34,29z M29,20.338V14c0-2.761-2.239-5-5-5s-5,2.239-5,5 v6.338 M24,28.5V32"
        ]
        []
    ]


lockTwo : List (Svg msg)
lockTwo =
    [ circle
        [ cx "24"
        , cy "29.5"
        , r "1.5"
        ]
        []
    , path
        [ fill "none"
        , d "M29,21v-7c0-2.761-2.239-5-5-5s-5,2.239-5,5v7 M24,29.5V33"
        ]
        []
    , path
        [ fill "none"
        , d "M14 21H34V39H14z"
        ]
        []
    ]


lockedNetworkOne : List (Svg msg)
lockedNetworkOne =
    [ path
        [ fill "none"
        , d "M29,20 c0,2.761-2.239,5-5,5s-5-2.239-5-5s2.239-5,5-5S29,17.239,29,20z M27,12c0-1.657-1.343-3-3-3s-3,1.343-3,3 M21,12v4 M27,12v4 M37,35v-4H11v4 M37,35c-1.105,0-2,0.895-2,2s0.895,2,2,2s2-0.895,2-2S38.105,35,37,35z M24,27v8"
        ]
        []
    , circle
        [ fill "none"
        , cx "24"
        , cy "37"
        , r "2"
        ]
        []
    , circle
        [ fill "none"
        , cx "11"
        , cy "37"
        , r "2"
        ]
        []
    , path
        [ fill "none"
        , d "M24 19L24 21"
        ]
        []
    , circle
        [ cx "24"
        , cy "19"
        , r "1"
        ]
        []
    ]


lockedNetworkTwo : List (Svg msg)
lockedNetworkTwo =
    [ path
        [ fill "none"
        , d "M37,35v-4H11v4 M37,35 c-1.105,0-2,0.895-2,2s0.895,2,2,2s2-0.895,2-2S38.105,35,37,35z M24,27v8"
        ]
        []
    , circle
        [ fill "none"
        , cx "24"
        , cy "37"
        , r "2"
        ]
        []
    , circle
        [ fill "none"
        , cx "11"
        , cy "37"
        , r "2"
        ]
        []
    , path
        [ fill "none"
        , d "M24 19L24 21"
        ]
        []
    , circle
        [ cx "24"
        , cy "19"
        , r "1"
        ]
        []
    , path
        [ fill "none"
        , d "M26.5 15v-3.5c0-1.381-1.119-2.5-2.5-2.5s-2.5 1.119-2.5 2.5V15M19 15H29V25H19z"
        ]
        []
    ]


secureProfile : List (Svg msg)
secureProfile =
    [ path
        [ fill "none"
        , d "M31.5 30.499L31.5 33.415"
        ]
        []
    , circle
        [ cx "31.5"
        , cy "30.5"
        , r "1"
        ]
        []
    , path
        [ fill "none"
        , d "M24,27v12h15V24H9V9h15 v12.013 M34.999,24L35,19.5c0-1.925-1.575-3.5-3.5-3.5h-0.001C29.575,16,28,17.575,28,19.5V24 M21,24v-2.999 c0-2.059-1.409-3.787-3.304-4.317c0.775-0.425,1.307-1.238,1.307-2.184c0-1.381-1.119-2.5-2.5-2.5c-1.381,0-2.5,1.119-2.5,2.5 c0,0.945,0.531,1.758,1.305,2.183C13.41,17.212,12,18.941,12,21.001V24"
        ]
        []
    ]


security : List (Svg msg)
security =
    [ path
        [ fill "none"
        , d "M24,9l-11,6c0,0,0,8,0,13 c0,6,5,11,11,11s11-5,11-11c0-7,0-13,0-13L24,9z"
        ]
        []
    ]


unlockOne : List (Svg msg)
unlockOne =
    [ path
        [ fill "none"
        , d "M29,14 c0-2.761-2.239-5-5-5s-5,2.239-5,5v6.338"
        ]
        []
    , circle
        [ cx "24"
        , cy "28.5"
        , r "1.5"
        ]
        []
    , path
        [ fill "none"
        , d "M34,29c0,5.523-4.477,10-10,10s-10-4.477-10-10s4.477-10,10-10S34,23.477,34,29z M24,28.5V32"
        ]
        []
    ]


unlockTwo : List (Svg msg)
unlockTwo =
    [ circle
        [ cx "24"
        , cy "29.5"
        , r "1.5"
        ]
        []
    , path
        [ fill "none"
        , d "M24,29.5V33"
        ]
        []
    , path
        [ fill "none"
        , d "M14 21H34V39H14z"
        ]
        []
    , path
        [ fill "none"
        , d "M29,14 c0-2.761-2.239-5-5-5s-5,2.239-5,5v7"
        ]
        []
    ]
