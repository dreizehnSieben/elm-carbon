module UI.Spacing exposing (..)

import Element exposing (px, Length)


spacing01 : Length
spacing01 =
    px 2


spacing02 : Length
spacing02 =
    px 4


spacing03 : Length
spacing03 =
    px 8


spacing04 : Length
spacing04 =
    px 12


spacing05 : Length
spacing05 =
    px 16


spacing06 : Length
spacing06 =
    px 24


spacing07 : Length
spacing07 =
    px 32


spacing08 : Length
spacing08 =
    px 40


spacing09 : Length
spacing09 =
    px 48


layout01 : Length
layout01 =
    px 16


layout02 : Length
layout02 =
    px 24


layout03 : Length
layout03 =
    px 32


layout04 : Length
layout04 =
    px 48


layout05 : Length
layout05 =
    px 64


layout06 : Length
layout06 =
    px 96


layout07 : Length
layout07 =
    px 160