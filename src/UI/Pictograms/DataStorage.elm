module UI.Pictograms.DataStorage exposing (..)

import Svg exposing (Svg, circle, path)
import Svg.Attributes exposing (cx, cy, d, fill, r)


archive : List (Svg msg)
archive =
    [ path
        [ fill "none"
        , d "M40,18H10v-8h30V18z M12,18v21c0,0.552,0.448,1,1,1h24c0.552,0,1-0.448,1-1V18 M30,23L30,23c0-1.1-0.9-2-2-2h-6c-1.1,0-2,0.9-2,2v0 c0,1.1,0.9,2,2,2h6C29.1,25,30,24.1,30,23z"
        ]
        []
    ]


dataBackup : List (Svg msg)
dataBackup =
    [ path
        [ fill "none"
        , d "M14 12H34V18H14zM15 15L21.5 15"
        ]
        []
    , circle
        [ cx "31.5", cy "15", r ".5" ]
        []
    , path
        [ fill "none"
        , d "M14 21H34V27H14zM15 24L21.5 24"
        ]
        []
    , circle
        [ cx "31.5", cy "24", r ".5" ]
        []
    , path
        [ fill "none"
        , d "M14 30H34V36H14zM15 33L21.5 33"
        ]
        []
    , circle
        [ cx "31.5", cy "33", r ".5" ]
        []
    , path
        [ fill "none"
        , d "M34 15L38 15 38 24 34 24M14 33L10 33 10 24 14 24"
        ]
        []
    ]


dataProcessing : List (Svg msg)
dataProcessing =
    [ path
        [ fill "none"
        , d "M10 10H24V15H10zM12 12L18 12M10 17H24V22H10zM12 19L18 19"
        ]
        []
    , circle
        [ cx "21.5", cy "12", r ".5" ]
        []
    , circle
        [ cx "21.5", cy "19", r ".5" ]
        []
    , path
        [ fill "none"
        , d "M24 26H38V31H24zM26 28L32 28M24 33H38V38H24zM26 35L32 35"
        ]
        []
    , circle
        [ cx "35.5", cy "28", r ".5" ]
        []
    , circle
        [ cx "35.5", cy "35", r ".5" ]
        []
    , path
        [ fill "none"
        , d "M26 16L36 16 36 24M22 32L12 32 12 24"
        ]
        []
    ]


dataStorage : List (Svg msg)
dataStorage =
    [ path
        [ fill "none"
        , d "M39,34v5H9v-5 M9,26v5h30v-5 M9,18v5h30v-5 M9,10v5h30v-5"
        ]
        []
    ]


hardDrive : List (Svg msg)
hardDrive =
    [ path
        [ d "M34,14c-0.552,0-1-0.448-1-1s0.448-1,1-1s1,0.448,1,1S34.552,14,34,14z M35,24c0-0.552-0.448-1-1-1c-0.552,0-1,0.448-1,1 s0.448,1,1,1C34.552,25,35,24.552,35,24z M35,35c0-0.552-0.448-1-1-1s-1,0.448-1,1s0.448,1,1,1S35,35.552,35,35z" ]
        []
    , path
        [ fill "none"
        , d "M39,17H9V9h30V17z M39,20H9v8h30V20z M39,31H9v8h30V31z"
        ]
        []
    ]


hardDriveNetwork : List (Svg msg)
hardDriveNetwork =
    [ path
        [ d "M27.999,14c-0.552,0-1-0.448-1-1c0-0.552,0.448-1,1-1s1,0.448,1,1C28.999,13.552,28.551,14,27.999,14z M29,24.001 c0-0.552-0.448-1-1-1s-1,0.448-1,1c0,0.552,0.448,1,1,1S29,24.553,29,24.001z M29.001,35.002c0-0.552-0.448-1-1-1 c-0.552,0-1,0.448-1,1s0.448,1,1,1C28.553,36.002,29.001,35.554,29.001,35.002z" ]
        []
    , path
        [ fill "none"
        , d "M32,17H9V9h23V17z M32,20H9v8h23V20z M32,31H9v8h23V31z M34,24h5 M34,35l5,0V13h-5"
        ]
        []
    ]


serverRack : List (Svg msg)
serverRack =
    [ path
        [ fill "none"
        , d "M35,39H13v-5h22V39z M27,36h6 M15,36h1 M17,36h1 M19,36h1 M35,29H13v5h22V29z M27,31h6 M15,31h1 M17,31h1 M19,31h1 M35,24H13v5h22 V24z M27,26h6 M15,26h1 M17,26h1 M19,26h1 M35,19H13v5h22V19z M27,21h6 M15,21h1 M17,21h1 M19,21h1 M35,14H13v5h22V14z M27,16h6 M15,16h1 M17,16h1 M19,16h1 M35,9H13v5h22V9z M27,11h6 M15,11h1 M17,11h1 M19,11h1"
        ]
        []
    ]


servers : List (Svg msg)
servers =
    [ path
        [ fill "none"
        , d "M31,39 H17V9h14V39z M20,12h8 M20,15h8 M20,18h8 M31,35h8V13h-8 M31,16h4 M31,19h4 M31,22h4 M17,13H9v22h8 M17,16h-4 M17,19h-4 M17,22h-4"
        ]
        []
    ]
