module UI.Pictograms.SupplyChain exposing (..)

import Svg exposing (Svg, circle, path)
import Svg.Attributes exposing (cx, cy, d, fill, r)


connectedNodesToTheCloud : List (Svg msg)
connectedNodesToTheCloud =
    [ path
        [ fill "none"
        , d "M26.5,36.5 c0,1.381-1.119,2.5-2.5,2.5c-1.381,0-2.5-1.119-2.5-2.5c0-1.381,1.119-2.5,2.5-2.5C25.381,34,26.5,35.119,26.5,36.5z M28,27v4 c0,1.105,0.896,2,2,2h4 M14,33h4c1.105,0,2-0.895,2-2v-4 M24,27v7"
        ]
        []
    , circle
        [ fill "none"
        , cx "12"
        , cy "33"
        , r "2"
        ]
        []
    , circle
        [ fill "none"
        , cx "36"
        , cy "33"
        , r "2"
        ]
        []
    , path
        [ fill "none"
        , d "M34.5,27 c2.485,0,4.5-2.015,4.5-4.5c0-2.485-2.015-4.5-4.5-4.5H34c0-4.971-4.029-9-9-9c-3.529,0-6.566,2.042-8.041,5H15.5 C11.91,14,9,16.91,9,20.5c0,3.59,2.91,6.5,6.5,6.5H34.5"
        ]
        []
    ]


managingContractualFlow : List (Svg msg)
managingContractualFlow =
    [ path
        [ fill "none"
        , d "M36,10v5h-5 M17,33h-5v5 M36,15 c-2.737-3.643-7.093-6-12-6C15.716,9,9,15.716,9,24 M12,33c2.737,3.643,7.093,6,12,6c8.284,0,15-6.716,15-15"
        ]
        []
    , path
        [ fill "none"
        , d "M30 31L30 20 26 16 18 16 18 31z"
        ]
        []
    , path
        [ fill "none"
        , d "M26 16L26 20 30 20"
        ]
        []
    , path
        [ fill "none"
        , d "M20 25L28 25M20 27L28 27M20 23L28 23"
        ]
        []
    ]


managingItems : List (Svg msg)
managingItems =
    [ path
        [ fill "none"
        , d "M37 14h-7.779M20 9C13.925 9 9 13.925 9 20c0 6.075 4.925 11 11 11 6.076 0 11-4.925 11-11C31 13.925 26.076 9 20 9zM27.785 27.784L39 39M29.799 15L21 15M17 15L14 15M31 20L14 20M29.799 25L24 25M20 25L14 25M37 18L30.819 18M37 22L30.817 22M37 17H39V19H37zM26 35L14 35M26 34H28V36H26z"
        ]
        []
    , path
        [ fill "none"
        , d "M20 23H24V27H20zM17 13H21V17H17z"
        ]
        []
    ]


monitoredItemOnConveyor : List (Svg msg)
monitoredItemOnConveyor =
    [ circle
        [ fill "none"
        , cx "12"
        , cy "32"
        , r "2"
        ]
        []
    , circle
        [ fill "none"
        , cx "20"
        , cy "32"
        , r "2"
        ]
        []
    , circle
        [ fill "none"
        , cx "28"
        , cy "32"
        , r "2"
        ]
        []
    , circle
        [ fill "none"
        , cx "36"
        , cy "32"
        , r "2"
        ]
        []
    , path
        [ fill "none"
        , d "M9 28L39 28M9 36L39 36"
        ]
        []
    , path
        [ fill "none"
        , d "M20 18H28V26H20zM30 11H38V19H30zM10 18H18V26H10z"
        ]
        []
    , path
        [ fill "none"
        , d "M38 11L30 19M35 11L30 16M32 11L30 13M38 14L33 19M38 17L36 19"
        ]
        []
    ]


movementInOverlappingNetworks : List (Svg msg)
movementInOverlappingNetworks =
    [ path
        [ fill "none"
        , d "M19,34c-5.522,0-10-4.477-10-10 c0-5.523,4.478-10,10-10c5.522,0,10,4.477,10,10c0,3.701-2.011,6.933-5,8.662"
        ]
        []
    , path
        [ fill "none"
        , d "M15,21c-1.061,1.368-2,3.351-2,5.417 C13,30.605,15.687,34,19,34"
        ]
        []
    , path
        [ fill "none"
        , d "M12.084 21.662L15.001 20.999 15.663 23.925M29 14c5.522 0 10 4.477 10 10 0 5.523-4.478 10-10 10-5.522 0-10-4.477-10-10 0-3.701 2.011-6.933 5-8.662"
        ]
        []
    , path
        [ fill "none"
        , d "M33,27c1.061-1.368,2-3.351,2-5.417 C35,17.395,32.313,14,29,14"
        ]
        []
    , path
        [ fill "none"
        , d "M35.916 26.338L32.999 27.001 32.337 24.075"
        ]
        []
    ]


movementOfGoodsOne : List (Svg msg)
movementOfGoodsOne =
    [ path
        [ fill "none"
        , d "M22 24L9 24M39 24L26 24M29 18L13 18M18 12L10 12M30 12L22 12M18 10H22V14H18zM18 36L10 36M30 36L22 36M18 34H22V38H18zM22 22H26V26H22zM29 16H33V20H29zM29 30L13 30M29 28H33V32H29z"
        ]
        []
    , path
        [ fill "none"
        , d "M36 27L39 24 36 21"
        ]
        []
    ]


movementOfGoodsTwo : List (Svg msg)
movementOfGoodsTwo =
    [ path
        [ fill "none"
        , d "M14 22H18V26H14zM24 22H28V26H24zM39 24L28 24M14 24L9 24M24 24L18 24"
        ]
        []
    , path
        [ fill "none"
        , d "M36 27L39 24 36 21"
        ]
        []
    , path
        [ fill "none"
        , d "M31 17L31 17c0-2.761-2.239-5-5-5h-3M30 18L13 18M21 12L17 12M30 17H32V19H30zM31 31L31 31c0 2.761-2.239 5-5 5h-3M30 30L13 30M21 36L17 36M30 29H32V31H30zM21 11H23V13H21zM21 35H23V37H21z"
        ]
        []
    ]


movementOfGoodsThree : List (Svg msg)
movementOfGoodsThree =
    [ path
        [ fill "none"
        , d "M22 24L9 24M35 24L26.005 24M28 19L9 19M19 15L14 15M25 11c6.545 0 12 3.987 12 11"
        ]
        []
    , path
        [ fill "none"
        , d "M35 22H39V26H35z"
        ]
        []
    , path
        [ fill "none"
        , d "M21 15h2.309c2.265 0 4.433 1.117 5.691 3M28 29L9 29M19 33L14 33M25 37c6.545 0 12-3.987 12-11M21 33h2.309c2.265 0 4.433-1.117 5.691-3"
        ]
        []
    , circle
        [ fill "none"
        , cx "24"
        , cy "11"
        , r "1"
        ]
        []
    , circle
        [ fill "none"
        , cx "20"
        , cy "15"
        , r "1"
        ]
        []
    , circle
        [ fill "none"
        , cx "29"
        , cy "19"
        , r "1"
        ]
        []
    , circle
        [ fill "none"
        , cx "29"
        , cy "29"
        , r "1"
        ]
        []
    , circle
        [ fill "none"
        , cx "20"
        , cy "33"
        , r "1"
        ]
        []
    , circle
        [ fill "none"
        , cx "24"
        , cy "37"
        , r "1"
        ]
        []
    , circle
        [ fill "none"
        , cx "24"
        , cy "24"
        , r "1"
        ]
        []
    ]


movementOfItems : List (Svg msg)
movementOfItems =
    [ path
        [ fill "none"
        , d "M34.606 13.394C31.892 10.68 28.142 9 24 9 15.716 9 9 15.716 9 24M13.394 34.606C16.108 37.321 19.857 39 24 39c8.284 0 15-6.716 15-15M14 31l-4-4 4-4M25 27H10M34 17l4 4-4 4M23 21h15"
        ]
        []
    , circle
        [ fill "none"
        , cx "27.5"
        , cy "27"
        , r "2.5"
        ]
        []
    , path
        [ fill "none"
        , d "M19 19H23V23H19z"
        ]
        []
    ]


process : List (Svg msg)
process =
    [ path
        [ fill "none"
        , d "M24,27 c-1.657,0-3-1.343-3-3c0-1.657,1.343-3,3-3c1.657,0,3,1.343,3,3C27,25.657,25.657,27,24,27 M31,25.38v-2.76h-1.812l-0.54-1.316 l1.278-1.278l-1.952-1.952l-1.282,1.282l-1.312-0.549V17h-2.76v1.812l-1.316,0.54l-1.278-1.278l-1.951,1.952l1.281,1.282 l-0.549,1.312H17v2.76h1.812l0.54,1.316l-1.277,1.278l1.951,1.952l1.282-1.282l1.312,0.549V31h2.76v-1.812l1.316-0.54 l1.278,1.278l1.952-1.952l-1.282-1.282l0.549-1.312H31z"
        ]
        []
    , path
        [ fill "none"
        , d "M36,10v5h-5 M17,33h-5v5 M36,15 c-2.737-3.643-7.093-6-12-6C15.716,9,9,15.716,9,24 M12,33c2.737,3.643,7.093,6,12,6c8.284,0,15-6.716,15-15"
        ]
        []
    ]
