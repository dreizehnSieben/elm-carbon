module UI.Colors exposing (..)

import Element exposing (Color, rgb255)


red100 : Color
red100 =
    rgb255 45 7 9


red90 : Color
red90 =
    rgb255 82 4 8


red80 : Color
red80 =
    rgb255 117 14 19


red70 : Color
red70 =
    rgb255 162 25 31


red60 : Color
red60 =
    rgb255 218 30 40

red60hover : Color
red60hover =
    rgb255 186 27 35

red50 : Color
red50 =
    rgb255 250 77 86


red40 : Color
red40 =
    rgb255 255 131 137


red30 : Color
red30 =
    rgb255 255 179 184


red20 : Color
red20 =
    rgb255 255 215 217


red10 : Color
red10 =
    rgb255 255 241 241


magenta100 : Color
magenta100 =
    rgb255 42 10 24


magenta90 : Color
magenta90 =
    rgb255 81 2 36


magenta80 : Color
magenta80 =
    rgb255 114 9 55


magenta70 : Color
magenta70 =
    rgb255 159 24 83


magenta60 : Color
magenta60 =
    rgb255 208 38 112


magenta50 : Color
magenta50 =
    rgb255 238 83 150


magenta40 : Color
magenta40 =
    rgb255 238 83 150


magenta30 : Color
magenta30 =
    rgb255 255 175 210


magenta20 : Color
magenta20 =
    rgb255 255 214 232


magenta10 : Color
magenta10 =
    rgb255 255 240 247


purple100 : Color
purple100 =
    rgb255 28 15 48


purple90 : Color
purple90 =
    rgb255 49 19 94


purple80 : Color
purple80 =
    rgb255 73 29 139


purple70 : Color
purple70 =
    rgb255 105 41 196


purple60 : Color
purple60 =
    rgb255 138 63 252


purple50 : Color
purple50 =
    rgb255 165 110 255


purple40 : Color
purple40 =
    rgb255 190 149 255


purple30 : Color
purple30 =
    rgb255 212 187 255


purple20 : Color
purple20 =
    rgb255 232 218 255


purple10 : Color
purple10 =
    rgb255 246 242 255


blue100 : Color
blue100 =
    rgb255 0 17 65


blue90 : Color
blue90 =
    rgb255 0 29 108


blue80 : Color
blue80 =
    rgb255 0 45 156


blue70 : Color
blue70 =
    rgb255 0 67 206


blue60 : Color
blue60 =
    rgb255 15 98 254

blue60hover: Color
blue60hover =
    rgb255 3 83 233

blue50 : Color
blue50 =
    rgb255 69 137 255


blue40 : Color
blue40 =
    rgb255 120 169 255


blue30 : Color
blue30 =
    rgb255 166 200 255


blue20 : Color
blue20 =
    rgb255 208 226 255


blue10 : Color
blue10 =
    rgb255 237 245 255


cyan100 : Color
cyan100 =
    rgb255 6 23 39


cyan90 : Color
cyan90 =
    rgb255 1 39 73


cyan80 : Color
cyan80 =
    rgb255 0 58 109


cyan70 : Color
cyan70 =
    rgb255 0 83 154


cyan60 : Color
cyan60 =
    rgb255 0 114 195


cyan50 : Color
cyan50 =
    rgb255 17 146 232


cyan40 : Color
cyan40 =
    rgb255 51 177 255


cyan30 : Color
cyan30 =
    rgb255 130 207 255


cyan20 : Color
cyan20 =
    rgb255 186 230 255


cyan10 : Color
cyan10 =
    rgb255 229 246 255


teal100 : Color
teal100 =
    rgb255 8 26 28


teal90 : Color
teal90 =
    rgb255 2 43 48


teal80 : Color
teal80 =
    rgb255 0 65 68


teal70 : Color
teal70 =
    rgb255 0 93 93


teal60 : Color
teal60 =
    rgb255 0 125 121


teal50 : Color
teal50 =
    rgb255 0 157 154


teal40 : Color
teal40 =
    rgb255 8 189 186


teal30 : Color
teal30 =
    rgb255 61 219 217


teal20 : Color
teal20 =
    rgb255 158 240 240


teal10 : Color
teal10 =
    rgb255 217 251 251


green100 : Color
green100 =
    rgb255 7 25 8


green90 : Color
green90 =
    rgb255 2 45 13


green80 : Color
green80 =
    rgb255 4 67 23


green70 : Color
green70 =
    rgb255 14 96 39


green60 : Color
green60 =
    rgb255 25 128 56


green50 : Color
green50 =
    rgb255 36 161 72


green40 : Color
green40 =
    rgb255 66 190 101


green30 : Color
green30 =
    rgb255 111 220 140


green20 : Color
green20 =
    rgb255 167 240 186


green10 : Color
green10 =
    rgb255 222 251 230


black : Color
black =
    rgb255 0 0 0


white : Color
white =
    rgb255 255 255 255


coolGray100 : Color
coolGray100 =
    rgb255 18 22 25


coolGray90 : Color
coolGray90 =
    rgb255 33 39 42


coolGray80 : Color
coolGray80 =
    rgb255 52 58 63


coolGray70 : Color
coolGray70 =
    rgb255 77 83 88


coolGray60 : Color
coolGray60 =
    rgb255 105 112 119


coolGray50 : Color
coolGray50 =
    rgb255 135 141 150


coolGray40 : Color
coolGray40 =
    rgb255 162 169 176


coolGray30 : Color
coolGray30 =
    rgb255 193 199 205


coolGray20 : Color
coolGray20 =
    rgb255 221 225 230


coolGray10 : Color
coolGray10 =
    rgb255 242 244 248


gray100 : Color
gray100 =
    rgb255 22 22 22


gray90 : Color
gray90 =
    rgb255 38 38 38

gray90hover : Color
gray90hover =
    rgb255 53 53 53
    
gray80 : Color
gray80 =
    rgb255 57 57 57

gray80hover : Color
gray80hover =
    rgb255 76 76 76


gray70 : Color
gray70 =
    rgb255 82 82 82

gray70hover : Color
gray70hover =
    rgb255 101 101 101

gray60 : Color
gray60 =
    rgb255 111 111 111

gray60hover : Color
gray60hover =
    rgb255 96 96 96

gray50 : Color
gray50 =
    rgb255 141 141 141


gray40 : Color
gray40 =
    rgb255 168 168 168


gray30 : Color
gray30 =
    rgb255 198 198 198


gray20 : Color
gray20 =
    rgb255 224 224 224

gray20hover : Color
gray20hover =
    rgb255 202 202 202


gray10 : Color
gray10 =
    rgb255 244 244 244

gray10hover : Color
gray10hover =
    rgb255 229 229 229

warmGray100 : Color
warmGray100 =
    rgb255 23 20 20


warmGray90 : Color
warmGray90 =
    rgb255 39 37 37


warmGray80 : Color
warmGray80 =
    rgb255 60 56 56


warmGray70 : Color
warmGray70 =
    rgb255 86 81 81


warmGray60 : Color
warmGray60 =
    rgb255 114 110 110


warmGray50 : Color
warmGray50 =
    rgb255 143 139 139


warmGray40 : Color
warmGray40 =
    rgb255 173 168 168


warmGray30 : Color
warmGray30 =
    rgb255 202 197 196


warmGray20 : Color
warmGray20 =
    rgb255 229 224 223


warmGray10 : Color
warmGray10 =
    rgb255 247 243 242

alert60 : Color
alert60 =
    rgb255 218 30 40

alert50 : Color
alert50 =
    rgb255 36 161 72

alert40 : Color
alert40 =
    rgb255 255 131 43

alert30 : Color
alert30 =
    rgb255 241 194 27