module UI.Icons.Senses exposing (..)

import Svg exposing (Svg, path)
import Svg.Attributes exposing (d)


cognitive : List (Svg msg)
cognitive =
    [ path
        [ d "M30,13A11,11,0,0,0,19,2H11a9,9,0,0,0-9,9v3a5,5,0,0,0,5,5H8.1A5,5,0,0,0,13,23h1.38l4,7,1.73-1-4-6.89A2,2,0,0,0,14.38,21H13a3,3,0,0,1,0-6h1V13H13a5,5,0,0,0-4.9,4H7a3,3,0,0,1-3-3V12H6A3,3,0,0,0,9,9V8H7V9a1,1,0,0,1-1,1H4.08A7,7,0,0,1,11,4h6V6a1,1,0,0,1-1,1H14V9h2a3,3,0,0,0,3-3V4a9,9,0,0,1,8.05,5H26a3,3,0,0,0-3,3v1h2V12a1,1,0,0,1,1-1h1.77A8.76,8.76,0,0,1,28,13v1a5,5,0,0,1-5,5H20v2h3a7,7,0,0,0,3-.68V21a3,3,0,0,1-3,3H22v2h1a5,5,0,0,0,5-5V18.89A7,7,0,0,0,30,14Z" ]
        []
    ]


hearing : List (Svg msg)
hearing =
    [ path
        [ d "M18,30V28A10.0114,10.0114,0,0,0,28,18h2A12.0134,12.0134,0,0,1,18,30Z" ]
        []
    , path
        [ d "M18,26V24a6.0066,6.0066,0,0,0,6-6h2A8.0092,8.0092,0,0,1,18,26Z" ]
        []
    , path
        [ d "M18 22V20a2.0023 2.0023 0 002-2h2A4.0042 4.0042 0 0118 22zM10 2a9.01 9.01 0 00-9 9H3a7 7 0 0114 0 7.09 7.09 0 01-3.501 6.1348L13 17.4229v3.0732a2.9354 2.9354 0 01-.9009 2.1514 4.1824 4.1824 0 01-4.6318 1.03A4.0918 4.0918 0 015 20H3a6.1156 6.1156 0 003.6694 5.5117 5.7822 5.7822 0 002.3145.4863A6.5854 6.5854 0 0013.4624 24.11 4.94 4.94 0 0015 20.4961V18.5537A9.1077 9.1077 0 0019 11 9.01 9.01 0 0010 2z" ]
        []
    , path
        [ d "M9.28,8.0825A3.0061,3.0061,0,0,1,13,11h2a4.9786,4.9786,0,0,0-1.8843-3.9111A5.0414,5.0414,0,0,0,8.835,6.1323,4.95,4.95,0,0,0,5.1323,9.835,5.0318,5.0318,0,0,0,7.436,15.2935,3.0777,3.0777,0,0,1,9,17.9229V20h2V17.9229a5.0608,5.0608,0,0,0-2.5371-4.3458A3.0016,3.0016,0,0,1,9.28,8.0825Z" ]
        []
    ]


idea : List (Svg msg)
idea =
    [ path
        [ d "M11 24H21V26H11zM13 28H19V30H13zM16 2A10 10 0 006 12a9.19 9.19 0 003.46 7.62c1 .93 1.54 1.46 1.54 2.38h2c0-1.84-1.11-2.87-2.19-3.86A7.2 7.2 0 018 12a8 8 0 0116 0 7.2 7.2 0 01-2.82 6.14c-1.07 1-2.18 2-2.18 3.86h2c0-.92.53-1.45 1.54-2.39A9.18 9.18 0 0026 12 10 10 0 0016 2z" ]
        []
    ]


movement : List (Svg msg)
movement =
    [ path
        [ d "M24,20l-1.41,1.41L26.17,25H10a4,4,0,0,1,0-8H22A6,6,0,0,0,22,5H5.83L9.41,1.41,8,0,2,6l6,6,1.41-1.41L5.83,7H22a4,4,0,0,1,0,8H10a6,6,0,0,0,0,12H26.17l-3.58,3.59L24,32l6-6Z" ]
        []
    ]


smell : List (Svg msg)
smell =
    [ path
        [ d "M22,15V10H20v7h2a3,3,0,0,1,0,6H21V21H19v2a3,3,0,0,1-6,0V21H11v2H10a3,3,0,0,1,0-6h2V9a3,3,0,0,1,3-3h1V4H15a5,5,0,0,0-5,5v6a5,5,0,0,0,0,10h1.42a5,5,0,0,0,9.16,0H22a5,5,0,0,0,0-10Z" ]
        []
    ]


taste : List (Svg msg)
taste =
    [ path
        [ d "M4 4V6H8v8a8 8 0 0016 0V6h4V4zM22 14a6 6 0 01-12 0V6h5v8h2V6h5zM11 26H13V28H11zM7 24H9V26H7zM15 24H17V26H15zM19 26H21V28H19zM23 24H25V26H23z" ]
        []
    ]


touchOne : List (Svg msg)
touchOne =
    [ path
        [ d "M20,8H18A5,5,0,0,0,8,8H6A7,7,0,0,1,20,8Z" ]
        []
    , path
        [ d "M25,15a2.94,2.94,0,0,0-1.47.4A3,3,0,0,0,21,14a2.94,2.94,0,0,0-1.47.4A3,3,0,0,0,16,13.18V8h0a3,3,0,0,0-6,0V19.1L7.77,17.58h0A2.93,2.93,0,0,0,6,17a3,3,0,0,0-2.12,5.13l8,7.3A6.16,6.16,0,0,0,16,31h5a7,7,0,0,0,7-7V18A3,3,0,0,0,25,15Zm1,9a5,5,0,0,1-5,5H16a4.17,4.17,0,0,1-2.76-1L5.29,20.7A1,1,0,0,1,5,20a1,1,0,0,1,1.6-.8L12,22.9V8a1,1,0,0,1,2,0h0V19h2V16a1,1,0,0,1,2,0v3h2V17a1,1,0,0,1,2,0v2h2V18a1,1,0,0,1,2,0Z" ]
        []
    ]


touchTwo : List (Svg msg)
touchTwo =
    [ path
        [ d "M29,15H27A11,11,0,0,0,5,15H3a13,13,0,0,1,26,0Z" ]
        []
    , path
        [ d "M25,28H23V15A7,7,0,1,0,9,15V28H7V15a9,9,0,0,1,18,0Z" ]
        []
    , path
        [ d "M21,20H11V15a5,5,0,0,1,10,0Zm-8-2h6V15a3,3,0,0,0-6,0Z" ]
        []
    ]


touchInteraction : List (Svg msg)
touchInteraction =
    [ path
        [ d "M26,21V20a1,1,0,0,1,2,0V30h2V20a3.0033,3.0033,0,0,0-3-3,2.964,2.964,0,0,0-1.4708.4014,2.9541,2.9541,0,0,0-4-1A2.9934,2.9934,0,0,0,19,15a2.96,2.96,0,0,0-1,.1846L18,10h0a3,3,0,0,0-6,0V21.1045L9.7651,19.5752l-.0008.001a2.999,2.999,0,0,0-3.881,4.55L12.3223,30l1.3479-1.478L7.2915,22.7036A.9908.9908,0,0,1,7,22a1.0005,1.0005,0,0,1,1.6-.8008L14,24.8955V10a1,1,0,0,1,2,0h0V21h2V18a1,1,0,0,1,2,0v3h2V19a1,1,0,0,1,2,0v2Z" ]
        []
    , path
        [ d "M28,12H22V10h6V4H4v6H8v2H4a2.0021,2.0021,0,0,1-2-2V4A2.0021,2.0021,0,0,1,4,2H28a2.0021,2.0021,0,0,1,2,2v6A2.0021,2.0021,0,0,1,28,12Z" ]
        []
    ]
