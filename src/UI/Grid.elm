module UI.Grid exposing
    ( column1
    , column10
    , column11
    , column12
    , column13
    , column14
    , column15
    , column16
    , column2
    , column3
    , column4
    , column5
    , column6
    , column7
    , column8
    , column9
    , row
    )

import Element exposing (Attribute, Element, padding, width, fill, fillPortion)


row : List (Attribute msg) -> List (Element msg) -> Element msg
row =
    Element.row


column : List (Attribute msg) -> List (Element msg) -> Element msg
column attrs =
    Element.column <|
        attrs
            ++ [ padding 16 ]


column16 : List (Attribute msg) -> List (Element msg) -> Element msg
column16 attrs =
    column <| (++) [ width fill ] attrs


column15 : List (Attribute msg) -> List (Element msg) -> Element msg
column15 attrs =
    column <| (++) [ width <| fillPortion 15 ] attrs



column14 : List (Attribute msg) -> List (Element msg) -> Element msg
column14 attrs =
    column <| (++) [ width <| fillPortion 14 ] attrs

column13 : List (Attribute msg) -> List (Element msg) -> Element msg
column13 attrs =
    column <| (++) [ width <| fillPortion 13 ] attrs


column12 : List (Attribute msg) -> List (Element msg) -> Element msg
column12 attrs =
    column <| (++) [ width <| fillPortion 12 ] attrs


column11 : List (Attribute msg) -> List (Element msg) -> Element msg
column11 attrs =
    column <| (++) [ width <| fillPortion 11 ] attrs


column10 : List (Attribute msg) -> List (Element msg) -> Element msg
column10 attrs =
    column <| (++) [ width <| fillPortion 10 ] attrs


column9 : List (Attribute msg) -> List (Element msg) -> Element msg
column9 attrs =
    column <| (++) [ width <| fillPortion 9 ] attrs


column8 : List (Attribute msg ) -> List (Element msg) -> Element msg
column8 attrs =
    column <| (++) [ width <| fillPortion 8 ] attrs


column7 : List (Attribute msg) -> List (Element msg) -> Element msg
column7 attrs =
    column <| (++) [ width <| fillPortion 7 ] attrs


column6 : List (Attribute msg) -> List (Element msg) -> Element msg
column6 attrs =
    column <| (++) [ width <| fillPortion 6 ] attrs


column5 : List (Attribute msg) -> List (Element msg) -> Element msg
column5 attrs =
    column <| (++) [ width <| fillPortion 5 ] attrs


column4 : List (Attribute msg) -> List (Element msg) -> Element msg
column4 attrs =
    column <| (++) [ width <| fillPortion 4 ] attrs


column3 : List (Attribute msg) -> List (Element msg) -> Element msg
column3 attrs =
    column <| (++) [ width <| fillPortion 3 ] attrs


column2 : List (Attribute msg) -> List (Element msg) -> Element msg
column2 attrs =
    column <| (++) [ width <| fillPortion 2 ] attrs


column1 : List (Attribute msg) -> List (Element msg) -> Element msg
column1 attrs =
    column <| (++) [ width <| fillPortion 1 ] attrs