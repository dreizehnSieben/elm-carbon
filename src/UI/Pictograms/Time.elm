module UI.Pictograms.Time exposing (..)

import Svg exposing (Svg, path)
import Svg.Attributes exposing (d, fill)


calendar : List (Svg msg)
calendar =
    [ path
        [ fill "none"
        , d "M38,39 H10V12h28V39z M16,15V9 M24,15V9 M32,15V9 M25,19h-2v2h2V19z M25,26h-2v2h2V26z M25,33h-2v2h2V33z M17,19h-2v2h2V19z M17,26h-2v2h2 V26z M17,33h-2v2h2V33z M33,19h-2v2h2V19z M33,26h-2v2h2V26z M33,33h-2v2h2V33z"
        ]
        []
    ]


calendarDate : List (Svg msg)
calendarDate =
    [ path
        [ fill "none"
        , d "M38,39H10V12h28V39z M16,15V9 M32,15V9"
        ]
        []
    ]


calendarEvent : List (Svg msg)
calendarEvent =
    [ path
        [ fill "none"
        , d "M20,12h8 M14,12h-4v27h28V12h-4 M22,20h-8v4h8V20z M32,14v-4c0-0.552-0.448-1-1-1h0c-0.552,0-1,0.448-1,1v4c0,0.552,0.448,1,1,1h0 C31.553,15,32,14.552,32,14z M18,14v-4c0-0.552-0.448-1-1-1h0c-0.552,0-1,0.448-1,1v4c0,0.552,0.448,1,1,1h0 C17.552,15,18,14.552,18,14z M22,29h-8v4h8V29z M34,20h-8v4h8V20z M34,29h-8v4h8V29z"
        ]
        []
    ]


meter : List (Svg msg)
meter =
    [ path
        [ fill "none"
        , d "M39,24c0,8.284-6.716,15-15,15 S9,32.284,9,24S15.716,9,24,9S39,15.716,39,24z M24,11v3 M17.5,12.742L18.804,15 M12.742,17.5l1.758,1.015 M11,24h1.5 M12.742,30.5 l0.866-0.5 M35.258,30.5l-3.899-2.251 M37,24h-4 M35.258,17.5L24,24 M30.5,12.742L28.619,16"
        ]
        []
    ]


speedometer : List (Svg msg)
speedometer =
    [ path
        [ fill "none"
        , d "M24,12v5 M16.5,14.01 l2.5,4.33 M15.34,22l-4.33-2.5 M9,27h5 M11.01,34.5l4.33-2.5 M32.66,32l4.33,2.5 M39,27h-5 M37,19.5L24,27 M31.5,14.01L29,18.34"
        ]
        []
    ]


time : List (Svg msg)
time =
    [ path
        [ fill "none"
        , d "M39,24c0,8.284-6.716,15-15,15 S9,32.284,9,24S15.716,9,24,9S39,15.716,39,24z M24,12V9 M12,24H9 M24,39v-3 M36,24h3 M24,14v10l6.928,4"
        ]
        []
    ]


timeLapse : List (Svg msg)
timeLapse =
    [ path
        [ fill "none"
        , d "M17.072 28L24 24 24 13"
        ]
        []
    , path
        [ fill "none"
        , d "M39,36h-6v-6 M33.001,36.001 C36.644,33.264,39,28.907,39,24c0-8.284-6.716-15-15-15S9,15.716,9,24s6.716,15,15,15"
        ]
        []
    ]
