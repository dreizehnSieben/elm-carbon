module UI.Icons.Formatting exposing (..)

import Svg exposing (Svg, circle, path)
import Svg.Attributes exposing (cx, cy, d, fill, r, transform)


alignHorizontalCenter : List (Svg msg)
alignHorizontalCenter =
    [ path
        [ d "M24,18H17V14h3a2.0025,2.0025,0,0,0,2-2V8a2.0025,2.0025,0,0,0-2-2H17V2H15V6H12a2.0025,2.0025,0,0,0-2,2v4a2.0025,2.0025,0,0,0,2,2h3v4H8a2.0025,2.0025,0,0,0-2,2v4a2.0025,2.0025,0,0,0,2,2h7v4h2V26h7a2.0025,2.0025,0,0,0,2-2V20A2.0025,2.0025,0,0,0,24,18ZM12,8h8v4H12ZM24,24H8V20H24Z" ]
        []
    ]


alignHorizontalLeft : List (Svg msg)
alignHorizontalLeft =
    [ path
        [ d "M26 26H11a2.0023 2.0023 0 01-2-2V20a2.0023 2.0023 0 012-2H26a2.0023 2.0023 0 012 2v4A2.0023 2.0023 0 0126 26zm0-6.0012L11 20v4H26zM18 14H11a2.0023 2.0023 0 01-2-2V8a2.0023 2.0023 0 012-2h7a2.0023 2.0023 0 012 2v4A2.0023 2.0023 0 0118 14zm0-6.0012L11 8v4h7zM4 2H6V30H4z" ]
        []
    ]


alignHorizontalRight : List (Svg msg)
alignHorizontalRight =
    [ path
        [ d "M4 24V20a2.0023 2.0023 0 012-2H21a2.0023 2.0023 0 012 2v4a2.0023 2.0023 0 01-2 2H6A2.0023 2.0023 0 014 24zm2 0H21V20L6 19.9988zM12 12V8a2.0023 2.0023 0 012-2h7a2.0023 2.0023 0 012 2v4a2.0023 2.0023 0 01-2 2H14A2.0023 2.0023 0 0112 12zm2 0h7V8l-7-.0012z" ]
        []
    , path
        [ d "M26 2H28V30H26z", transform "rotate(-180 27 16)" ]
        []
    ]


alignVerticalBottom : List (Svg msg)
alignVerticalBottom =
    [ path
        [ d "M2 26H30V28H2zM24 23H20a2.0023 2.0023 0 01-2-2V14a2.0023 2.0023 0 012-2h4a2.0023 2.0023 0 012 2v7A2.0023 2.0023 0 0124 23zm-4-9v7h4.0012L24 14zM12 23H8a2.0023 2.0023 0 01-2-2V6A2.0023 2.0023 0 018 4h4a2.0023 2.0023 0 012 2V21A2.0023 2.0023 0 0112 23zM8 6V21h4.0012L12 6z" ]
        []
    ]


alignVerticalCenter : List (Svg msg)
alignVerticalCenter =
    [ path
        [ d "M30,15H26V12a2.0025,2.0025,0,0,0-2-2H20a2.0025,2.0025,0,0,0-2,2v3H14V8a2.0025,2.0025,0,0,0-2-2H8A2.0025,2.0025,0,0,0,6,8v7H2v2H6v7a2.0025,2.0025,0,0,0,2,2h4a2.0025,2.0025,0,0,0,2-2V17h4v3a2.0025,2.0025,0,0,0,2,2h4a2.0025,2.0025,0,0,0,2-2V17h4ZM8,24V8h4l.0012,16Zm12-4V12h4l.0012,8Z" ]
        []
    ]


alignVerticalTop : List (Svg msg)
alignVerticalTop =
    [ path
        [ d "M24 20H20a2.0023 2.0023 0 01-2-2V11a2.0023 2.0023 0 012-2h4a2.0023 2.0023 0 012 2v7A2.0023 2.0023 0 0124 20zm-4-9v7h4.0012L24 11zM12 28H8a2.0023 2.0023 0 01-2-2V11A2.0023 2.0023 0 018 9h4a2.0023 2.0023 0 012 2V26A2.0023 2.0023 0 0112 28zM8 11V26h4.0012L12 11zM2 4H30V6H2z" ]
        []
    ]


attachment : List (Svg msg)
attachment =
    [ path
        [ d "M28.1,18.9L13.1,3.9c-2.5-2.6-6.6-2.6-9.2-0.1S1.3,10.5,3.9,13c0,0,0.1,0.1,0.1,0.1L6.8,16l1.4-1.4l-2.9-2.9\tC3.6,10,3.6,7.1,5.3,5.4s4.6-1.8,6.3-0.1c0,0,0,0,0.1,0.1l14.9,14.9c1.8,1.7,1.8,4.6,0.1,6.3c-1.7,1.8-4.6,1.8-6.3,0.1\tc0,0,0,0-0.1-0.1l-7.4-7.4c-1-1-0.9-2.6,0-3.5c1-0.9,2.5-0.9,3.5,0l4.1,4.1l1.4-1.4c0,0-4.2-4.2-4.2-4.2c-1.8-1.7-4.6-1.6-6.3,0.2\tc-1.6,1.7-1.6,4.4,0,6.2l7.5,7.5c2.5,2.6,6.6,2.6,9.2,0.1S30.7,21.5,28.1,18.9C28.1,19,28.1,18.9,28.1,18.9L28.1,18.9z" ]
        []
    ]


brightnessContrast : List (Svg msg)
brightnessContrast =
    [ path
        [ d "M15 2H17V5H15zM27 15H30V17H27zM15 27H17V30H15zM2 15H5V17H2z" ]
        []
    , path
        [ d "M6.22 5.73H8.219999999999999V8.73H6.22z", transform "rotate(-45 7.227 7.236)" ]
        []
    , path
        [ d "M23.27 6.23H26.27V8.23H23.27z", transform "rotate(-45 24.766 7.232)" ]
        []
    , path
        [ d "M23.77 23.27H25.77V26.27H23.77z", transform "rotate(-45 24.77 24.77)" ]
        []
    , path
        [ d "M5.47 25.13L7.59 23 9 24.42 6.88 26.54 5.47 25.13zM16 8a8 8 0 108 8A8 8 0 0016 8zm0 14a6 6 0 010-12z" ]
        []
    ]


colorPalette : List (Svg msg)
colorPalette =
    [ circle
        [ cx "10", cy "12", r "2" ]
        []
    , circle
        [ cx "16", cy "9", r "2" ]
        []
    , circle
        [ cx "22", cy "12", r "2" ]
        []
    , circle
        [ cx "23", cy "18", r "2" ]
        []
    , circle
        [ cx "19", cy "23", r "2" ]
        []
    , path
        [ d "M16.54,2A14,14,0,0,0,2,16a4.82,4.82,0,0,0,6.09,4.65l1.12-.31A3,3,0,0,1,13,23.24V27a3,3,0,0,0,3,3A14,14,0,0,0,30,15.46,14.05,14.05,0,0,0,16.54,2Zm8.11,22.31A11.93,11.93,0,0,1,16,28a1,1,0,0,1-1-1V23.24a5,5,0,0,0-5-5,5.07,5.07,0,0,0-1.33.18l-1.12.31A2.82,2.82,0,0,1,4,16,12,12,0,0,1,16.47,4,12.18,12.18,0,0,1,28,15.53,11.89,11.89,0,0,1,24.65,24.32Z" ]
        []
    ]


colorSwitch : List (Svg msg)
colorSwitch =
    [ path
        [ d "M26,4H6A2.0025,2.0025,0,0,0,4,6V26a2.0025,2.0025,0,0,0,2,2H26a2.0025,2.0025,0,0,0,2-2V6A2.0025,2.0025,0,0,0,26,4ZM6,26,26,6V26Z" ]
        []
    ]


contrast : List (Svg msg)
contrast =
    [ path
        [ d "M29.37,11.84a13.6,13.6,0,0,0-1.06-2.51A14.17,14.17,0,0,0,25.9,6.1a14,14,0,1,0,0,19.8,14.17,14.17,0,0,0,2.41-3.23,13.6,13.6,0,0,0,1.06-2.51,14,14,0,0,0,0-8.32ZM4,16A12,12,0,0,1,16,4V28A12,12,0,0,1,4,16Z" ]
        []
    ]


corner : List (Svg msg)
corner =
    [ path
        [ d "M28,9H14V6H6v8H9V28h2V14h3V11H28ZM12,12H8V8h4Z" ]
        []
    ]


crop : List (Svg msg)
crop =
    [ path
        [ d "M25,20H23V9H12V7H23a2,2,0,0,1,2,2Z" ]
        []
    , path
        [ d "M9,23V2H7V7H2V9H7V23a2,2,0,0,0,2,2H23v5h2V25h5V23Z" ]
        []
    ]


cut : List (Svg msg)
cut =
    [ path
        [ d "M26.5,19.63,20.24,16l6.26-3.63a5,5,0,0,0-1.21-9.2A5.19,5.19,0,0,0,24,3a5,5,0,0,0-4.33,7.53,5,5,0,0,0,2.39,2.1l-3.82,2.21L4,6.6,3,8.34,16.24,16,3,23.68l1,1.74,14.24-8.26,3.82,2.21a5,5,0,0,0-2.39,2.1A5,5,0,0,0,24,29a5.19,5.19,0,0,0,1.29-.17,5,5,0,0,0,1.21-9.2ZM21.4,9.53a3,3,0,0,1,1.1-4.12,3,3,0,0,1,4.1,1.11,3,3,0,0,1-1.1,4.11h0A3,3,0,0,1,21.4,9.53Zm5.2,16a3,3,0,0,1-4.1,1.11,3,3,0,0,1-1.1-4.12,3,3,0,0,1,4.1-1.1h0A3,3,0,0,1,26.6,25.48Z" ]
        []
    ]


distributeHorizontalCenter : List (Svg msg)
distributeHorizontalCenter =
    [ path
        [ d "M24 10H23V2H21v8H20a2.0023 2.0023 0 00-2 2v8a2.0023 2.0023 0 002 2h1v8h2V22h1a2.0023 2.0023 0 002-2V12A2.0023 2.0023 0 0024 10zm0 10H20V12h4zM12 6H11V2H9V6H8A2.0023 2.0023 0 006 8V24a2.0023 2.0023 0 002 2H9v4h2V26h1a2.0023 2.0023 0 002-2V8A2.0023 2.0023 0 0012 6zm0 18H8V8h4z" ]
        []
    ]


distributeHorizontalLeft : List (Svg msg)
distributeHorizontalLeft =
    [ path
        [ d "M28 22H24a2.0021 2.0021 0 01-2-2V12a2.0021 2.0021 0 012-2h4a2.0021 2.0021 0 012 2v8A2.0021 2.0021 0 0128 22zM24 12v8h4V12zM18 2H20V30H18zM12 26H8a2.0021 2.0021 0 01-2-2V8A2.0021 2.0021 0 018 6h4a2.0021 2.0021 0 012 2V24A2.0021 2.0021 0 0112 26zM8 8V24h4V8zM2 2H4V30H2z" ]
        []
    ]


distributeHorizontalRight : List (Svg msg)
distributeHorizontalRight =
    [ path
        [ d "M28 2H30V30H28zM24 22H20a2.0021 2.0021 0 01-2-2V12a2.0021 2.0021 0 012-2h4a2.0021 2.0021 0 012 2v8A2.0021 2.0021 0 0124 22zM20 12h-.0015L20 20h4V12zM12 2H14V30H12zM8 26H4a2.0021 2.0021 0 01-2-2V8A2.0021 2.0021 0 014 6H8a2.0021 2.0021 0 012 2V24A2.0021 2.0021 0 018 26zM4 8H3.9985L4 24H8V8z" ]
        []
    ]


distributeVerticalBottom : List (Svg msg)
distributeVerticalBottom =
    [ path
        [ d "M2 28H30V30H2zM24 26H8a2.0021 2.0021 0 01-2-2V20a2.0021 2.0021 0 012-2H24a2.0021 2.0021 0 012 2v4A2.0021 2.0021 0 0124 26zm0-6.0012L8 20v4H24zM2 12H30V14H2zM20 10H12a2.0021 2.0021 0 01-2-2V4a2.0021 2.0021 0 012-2h8a2.0021 2.0021 0 012 2V8A2.0021 2.0021 0 0120 10zm0-6.0012L12 4V8h8z" ]
        []
    ]


distributeVerticalCenter : List (Svg msg)
distributeVerticalCenter =
    [ path
        [ d "M30 21H26V20a2.0023 2.0023 0 00-2-2H8a2.0023 2.0023 0 00-2 2v1H2v2H6v1a2.0023 2.0023 0 002 2H24a2.0023 2.0023 0 002-2V23h4zm-6 3H8V20l16-.001zM30 9H22V8a2.0023 2.0023 0 00-2-2H12a2.0023 2.0023 0 00-2 2V9H2v2h8v1a2.0023 2.0023 0 002 2h8a2.0023 2.0023 0 002-2V11h8zM20 12H12V8l8-.001z" ]
        []
    ]


distributeVerticalTop : List (Svg msg)
distributeVerticalTop =
    [ path
        [ d "M24 30H8a2.0021 2.0021 0 01-2-2V24a2.0021 2.0021 0 012-2H24a2.0021 2.0021 0 012 2v4A2.0021 2.0021 0 0124 30zM8 24v4H24V24zM2 18H30V20H2zM20 14H12a2.0021 2.0021 0 01-2-2V8a2.0021 2.0021 0 012-2h8a2.0021 2.0021 0 012 2v4A2.0021 2.0021 0 0120 14zM12 8v4h8V8zM2 2H30V4H2z" ]
        []
    ]


dotMark : List (Svg msg)
dotMark =
    [ circle
        [ cx "16", cy "16", r "8" ]
        []
    ]


draw : List (Svg msg)
draw =
    [ path
        [ d "M19.14,28a3.42,3.42,0,0,1-2.42-5.85L21.86,17a1.42,1.42,0,1,0-2-2L13,21.85a3.5,3.5,0,0,1-4.85,0,3.43,3.43,0,0,1,0-4.84l8.58-8.58a1.42,1.42,0,1,0-2-2L6.41,14.7,5,13.3,13.29,5a3.43,3.43,0,0,1,4.84,4.85L9.56,18.42a1.42,1.42,0,0,0,0,2,1.45,1.45,0,0,0,2,0l6.86-6.86a3.43,3.43,0,1,1,4.85,4.84l-5.15,5.15a1.42,1.42,0,0,0,2,2l4.44-4.43L26,22.56,21.56,27A3.38,3.38,0,0,1,19.14,28Z" ]
        []
    ]


dropPhoto : List (Svg msg)
dropPhoto =
    [ path
        [ d "M19,26a4,4,0,1,1,4-4A4.0045,4.0045,0,0,1,19,26Zm0-6a2,2,0,1,0,2,2A2.0021,2.0021,0,0,0,19,20Z" ]
        []
    , path
        [ d "M27 29H11a2 2 0 01-2-2V18a2 2 0 012-2h3.2793l.5441-1.6324A2 2 0 0116.7208 13h4.5584a2 2 0 011.8974 1.3676L23.7207 16H27a2 2 0 012 2v9A2 2 0 0127 29zM11 27H27V18H22.2792l-1-3H16.7207l-1 3H11zM27 11H29V13H27zM27 7H29V9H27zM27 3H29V5H27zM23 3H25V5H23zM19 3H21V5H19zM15 3H17V5H15zM11 3H13V5H11zM7 3H9V5H7zM3 3H5V5H3zM3 7H5V9H3zM3 11H5V13H3zM3 15H5V17H3zM3 19H5V21H3zM3 23H5V25H3zM3 27H5V29H3z" ]
        []
    ]


dropPhotoFilled : List (Svg msg)
dropPhotoFilled =
    [ circle
        [ cx "19", cy "22", r "2" ]
        []
    , path
        [ d "M27,16H23.7207l-.5439-1.6328A1.9983,1.9983,0,0,0,21.2793,13H16.7207a1.9981,1.9981,0,0,0-1.8975,1.3677L14.2793,16H11a2.0023,2.0023,0,0,0-2,2v9a2.0023,2.0023,0,0,0,2,2H27a2.0023,2.0023,0,0,0,2-2V18A2.0023,2.0023,0,0,0,27,16ZM19,26a4,4,0,1,1,4-4A4.0045,4.0045,0,0,1,19,26Z" ]
        []
    , path
        [ fill "none"
        , d "M19,26a4,4,0,1,1,4-4A4.0045,4.0045,0,0,1,19,26Zm0-6a2,2,0,1,0,2,2A2.0021,2.0021,0,0,0,19,20Z"
        ]
        []
    , path
        [ d "M27 11H29V13H27zM27 7H29V9H27zM27 3H29V5H27zM23 3H25V5H23zM19 3H21V5H19zM15 3H17V5H15zM11 3H13V5H11zM7 3H9V5H7zM3 3H5V5H3zM3 7H5V9H3zM3 11H5V13H3zM3 15H5V17H3zM3 19H5V21H3zM3 23H5V25H3zM3 27H5V29H3z" ]
        []
    ]


edit : List (Svg msg)
edit =
    [ path
        [ d "M2 26H30V28H2zM25.4 9c.8-.8.8-2 0-2.8 0 0 0 0 0 0l-3.6-3.6c-.8-.8-2-.8-2.8 0 0 0 0 0 0 0l-15 15V24h6.4L25.4 9zM20.4 4L24 7.6l-3 3L17.4 7 20.4 4zM6 22v-3.6l10-10 3.6 3.6-10 10H6z" ]
        []
    ]


editOff : List (Svg msg)
editOff =
    [ path
        [ d "M30 28.6L3.4 2 2 3.4l10.1 10.1L4 21.6V28h6.4l8.1-8.1L28.6 30 30 28.6zM9.6 26H6v-3.6l7.5-7.5 3.6 3.6L9.6 26zM29.4 6.2L29.4 6.2l-3.6-3.6c-.8-.8-2-.8-2.8 0l0 0 0 0-8 8 1.4 1.4L20 8.4l3.6 3.6L20 15.6l1.4 1.4 8-8C30.2 8.2 30.2 7 29.4 6.2L29.4 6.2zM25 10.6L21.4 7l3-3L28 7.6 25 10.6z" ]
        []
    ]


erase : List (Svg msg)
erase =
    [ path
        [ d "M7 27H30V29H7zM27.38 10.51L19.45 2.59a2 2 0 00-2.83 0l-14 14a2 2 0 000 2.83L7.13 24h9.59L27.38 13.34A2 2 0 0027.38 10.51zM15.89 22H8L4 18l6.31-6.31 7.93 7.92zm3.76-3.76l-7.92-7.93L18 4 26 11.93z" ]
        []
    ]


eyedropper : List (Svg msg)
eyedropper =
    [ path
        [ d "M2 27H5V30H2zM29.71 7.29l-5-5a1 1 0 00-1.41 0h0L20 5.59l-1.29-1.3L17.29 5.71 18.59 7 8.29 17.29A1 1 0 008 18v1.59l-2.71 2.7a1 1 0 000 1.41h0l3 3a1 1 0 001.41 0h0L12.41 24H14a1 1 0 00.71-.29L25 13.41l1.29 1.3 1.42-1.42L26.41 12l3.3-3.29a1 1 0 000-1.41zM13.59 22h-2L9 24.59 7.41 23 10 20.41v-2l10-10L23.59 12zM25 10.59L21.41 7 24 4.41 27.59 8z" ]
        []
    ]


gradient : List (Svg msg)
gradient =
    [ path
        [ d "M26,4H6A2.0023,2.0023,0,0,0,4,6V26a2.0023,2.0023,0,0,0,2,2H26a2.0023,2.0023,0,0,0,2-2V6A2.0023,2.0023,0,0,0,26,4ZM22,26V22H18v4H14V22H10V18h4V14H10V10h4V6h4v4h4V6h4V26Z" ]
        []
    , path
        [ d "M14 10H18V14H14zM14 18H18V22H14zM18 14H22V18H18z" ]
        []
    ]


image : List (Svg msg)
image =
    [ path
        [ d "M19,14a3,3,0,1,0-3-3A3,3,0,0,0,19,14Zm0-4a1,1,0,1,1-1,1A1,1,0,0,1,19,10Z" ]
        []
    , path
        [ d "M26,4H6A2,2,0,0,0,4,6V26a2,2,0,0,0,2,2H26a2,2,0,0,0,2-2V6A2,2,0,0,0,26,4Zm0,22H6V20l5-5,5.59,5.59a2,2,0,0,0,2.82,0L21,19l5,5Zm0-4.83-3.59-3.59a2,2,0,0,0-2.82,0L18,19.17l-5.59-5.59a2,2,0,0,0-2.82,0L6,17.17V6H26Z" ]
        []
    ]


imageReference : List (Svg msg)
imageReference =
    [ path
        [ d "M4 20L4 22 8.586 22 2 28.586 3.414 30 10 23.414 10 28 12 28 12 20 4 20zM19 14a3 3 0 10-3-3A3 3 0 0019 14zm0-4a1 1 0 11-1 1A1 1 0 0119 10z" ]
        []
    , path
        [ d "M26,4H6A2,2,0,0,0,4,6V16H6V6H26V21.17l-3.59-3.59a2,2,0,0,0-2.82,0L18,19.17,11.8308,13l-1.4151,1.4155L14,18l2.59,2.59a2,2,0,0,0,2.82,0L21,19l5,5v2H16v2H26a2,2,0,0,0,2-2V6A2,2,0,0,0,26,4Z" ]
        []
    ]


language : List (Svg msg)
language =
    [ path
        [ d "M18 19H24V21H18zM18 15H30V17H18zM18 11H30V13H18zM14 21V19H9V17H7v2H2v2h8.2148a8.5914 8.5914 0 01-2.2159 3.9771A9.2731 9.2731 0 016.5521 23H4.3334a10.8553 10.8553 0 002.1451 3.2966A14.6584 14.6584 0 013 28.127L3.7021 30a16.42 16.42 0 004.2907-2.3362A16.4883 16.4883 0 0012.2979 30L13 28.127A14.664 14.664 0 019.5228 26.3 10.3132 10.3132 0 0012.2516 21zM11.1666 13H13.333L8.75 2H6.5832L2 13H4.1664L5 11h5.3335zM5.8331 9L7.6665 4.6 9.5 9z" ]
        []
    ]


lasso : List (Svg msg)
lasso =
    [ path
        [ d "M20,2H12A9.9842,9.9842,0,0,0,7.0349,20.6553C7.0249,20.7705,7,20.8818,7,21a3.9929,3.9929,0,0,0,2.9106,3.83A4.0049,4.0049,0,0,1,6,28H4v2H6a6.0044,6.0044,0,0,0,5.928-5.12,3.9966,3.9966,0,0,0,2.93-2.88H20A10,10,0,0,0,20,2ZM11,23a2,2,0,1,1,2-2A2.0025,2.0025,0,0,1,11,23Zm9-3H14.8579a3.9841,3.9841,0,0,0-7.15-1.2637A7.99,7.99,0,0,1,12,4h8a8,8,0,0,1,0,16Z" ]
        []
    ]


link : List (Svg msg)
link =
    [ path
        [ d "M29.25,6.76a6,6,0,0,0-8.5,0l1.42,1.42a4,4,0,1,1,5.67,5.67l-8,8a4,4,0,1,1-5.67-5.66l1.41-1.42-1.41-1.42-1.42,1.42a6,6,0,0,0,0,8.5A6,6,0,0,0,17,25a6,6,0,0,0,4.27-1.76l8-8A6,6,0,0,0,29.25,6.76Z" ]
        []
    , path
        [ d "M4.19,24.82a4,4,0,0,1,0-5.67l8-8a4,4,0,0,1,5.67,0A3.94,3.94,0,0,1,19,14a4,4,0,0,1-1.17,2.85L15.71,19l1.42,1.42,2.12-2.12a6,6,0,0,0-8.51-8.51l-8,8a6,6,0,0,0,0,8.51A6,6,0,0,0,7,28a6.07,6.07,0,0,0,4.28-1.76L9.86,24.82A4,4,0,0,1,4.19,24.82Z" ]
        []
    ]


listBoxes : List (Svg msg)
listBoxes =
    [ path
        [ d "M16 8H30V10H16zM16 22H30V24H16zM10 14H4a2.0023 2.0023 0 01-2-2V6A2.0023 2.0023 0 014 4h6a2.0023 2.0023 0 012 2v6A2.0023 2.0023 0 0110 14zM4 6v6h6.0012L10 6zM10 28H4a2.0023 2.0023 0 01-2-2V20a2.0023 2.0023 0 012-2h6a2.0023 2.0023 0 012 2v6A2.0023 2.0023 0 0110 28zM4 20v6h6.0012L10 20z" ]
        []
    ]


listBulleted : List (Svg msg)
listBulleted =
    [ circle
        [ cx "7", cy "9", r "3" ]
        []
    , circle
        [ cx "7", cy "23", r "3" ]
        []
    , path
        [ d "M16 22H30V24H16zM16 8H30V10H16z" ]
        []
    ]


listChecked : List (Svg msg)
listChecked =
    [ path
        [ d "M16 8H30V10H16zM6 10.59L3.41 8 2 9.41 6 13.41 14 5.41 12.59 4 6 10.59zM16 22H30V24H16zM6 24.59L3.41 22 2 23.41 6 27.41 14 19.41 12.59 18 6 24.59z" ]
        []
    ]


listNumbered : List (Svg msg)
listNumbered =
    [ path
        [ d "M16 22H30V24H16zM16 8H30V10H16zM8 12L8 4 6 4 6 5 4 5 4 7 6 7 6 12 4 12 4 14 6 14 8 14 10 14 10 12 8 12zM10 28H4V24a2 2 0 012-2H8V20H4V18H8a2 2 0 012 2v2a2 2 0 01-2 2H6v2h4z" ]
        []
    ]


magicWand : List (Svg msg)
magicWand =
    [ path
        [ d "M29.4141,24,12,6.5859a2.0476,2.0476,0,0,0-2.8281,0l-2.586,2.586a2.0021,2.0021,0,0,0,0,2.8281L23.999,29.4141a2.0024,2.0024,0,0,0,2.8281,0l2.587-2.5865a1.9993,1.9993,0,0,0,0-2.8281ZM8,10.5859,10.5859,8l5,5-2.5866,2.5869-5-5ZM25.4131,28l-11-10.999L17,14.4141l11,11Z" ]
        []
    , path
        [ d "M2.586 14.586H5.414V17.414H2.586z", transform "rotate(-45 4 16)" ]
        []
    , path
        [ d "M14.586 2.586H17.414V5.414H14.586z", transform "rotate(-45 16 4)" ]
        []
    , path
        [ d "M2.586 2.586H5.414V5.414H2.586z", transform "rotate(-45 4 4)" ]
        []
    ]


magicWandFilled : List (Svg msg)
magicWandFilled =
    [ path
        [ d "M29.4141,24,12,6.5859a2.0476,2.0476,0,0,0-2.8281,0l-2.586,2.586a2.0021,2.0021,0,0,0,0,2.8281L23.999,29.4141a2.0024,2.0024,0,0,0,2.8281,0l2.587-2.5865a1.9993,1.9993,0,0,0,0-2.8281ZM8,10.5859,10.5859,8l5,5-2.5866,2.5869-5-5Z" ]
        []
    , path
        [ d "M2.586 14.586H5.414V17.414H2.586z", transform "rotate(-45 4 16)" ]
        []
    , path
        [ d "M14.586 2.586H17.414V5.414H14.586z", transform "rotate(-45 16 4)" ]
        []
    , path
        [ d "M2.586 2.586H5.414V5.414H2.586z", transform "rotate(-45 4 4)" ]
        []
    ]


noImage : List (Svg msg)
noImage =
    [ path
        [ d "M30 3.4141L28.5859 2 2 28.5859 3.4141 30l2-2H26a2.0027 2.0027 0 002-2V5.4141zM26 26H7.4141l7.7929-7.793 2.3788 2.3787a2 2 0 002.8284 0L22 19l4 3.9973zm0-5.8318l-2.5858-2.5859a2 2 0 00-2.8284 0L19 19.1682l-2.377-2.3771L26 7.4141zM6 22V19l5-4.9966 1.3733 1.3733 1.4159-1.416-1.375-1.375a2 2 0 00-2.8284 0L6 16.1716V6H22V4H6A2.002 2.002 0 004 6V22z" ]
        []
    ]


opacity : List (Svg msg)
opacity =
    [ path
        [ d "M6 6H10V10H6zM10 10H14V14H10zM14 6H18V10H14zM22 6H26V10H22zM6 14H10V18H6zM14 14H18V18H14zM22 14H26V18H22zM6 22H10V26H6zM14 22H18V26H14zM22 22H26V26H22zM18 10H22V14H18zM10 18H14V22H10zM18 18H22V22H18z" ]
        []
    ]


paintBrush : List (Svg msg)
paintBrush =
    [ path
        [ d "M28.83,23.17,23,17.33V13a1,1,0,0,0-.29-.71l-10-10a1,1,0,0,0-1.42,0l-9,9a1,1,0,0,0,0,1.42l10,10A1,1,0,0,0,13,23h4.34l5.83,5.84a4,4,0,0,0,5.66-5.66ZM6,10.41l2.29,2.3,1.42-1.42L7.41,9,9,7.41l4.29,4.3,1.42-1.42L10.41,6,12,4.41,18.59,11,11,18.59,4.41,12Zm21.41,17a2,2,0,0,1-2.82,0l-6.13-6.12a1.8,1.8,0,0,0-.71-.29H13.41l-1-1L20,12.41l1,1v4.34a1,1,0,0,0,.29.7l6.12,6.14a2,2,0,0,1,0,2.82Z" ]
        []
    ]


paintBrushAlt : List (Svg msg)
paintBrushAlt =
    [ path
        [ d "M28.8281,3.1719a4.0941,4.0941,0,0,0-5.6562,0L4.05,22.292A6.9537,6.9537,0,0,0,2,27.2412V30H4.7559a6.9523,6.9523,0,0,0,4.95-2.05L28.8281,8.8286a3.999,3.999,0,0,0,0-5.6567ZM10.91,18.26l2.8286,2.8286L11.6172,23.21,8.7886,20.3818ZM8.2915,26.5356A4.9665,4.9665,0,0,1,4.7559,28H4v-.7588a4.9671,4.9671,0,0,1,1.4644-3.5351l1.91-1.91,2.8286,2.8281ZM27.4141,7.4141,15.1528,19.6748l-2.8286-2.8286,12.2617-12.26a2.0473,2.0473,0,0,1,2.8282,0,1.9995,1.9995,0,0,1,0,2.8282Z" ]
        []
    ]


paragraph : List (Svg msg)
paragraph =
    [ path
        [ d "M27,4H13a7,7,0,0,0,0,14V28h2V6h5V28h2V6h5ZM13,16A5,5,0,0,1,13,6Z" ]
        []
    ]


percentage : List (Svg msg)
percentage =
    [ path
        [ d "M9,14a5,5,0,1,1,5-5A5.0055,5.0055,0,0,1,9,14ZM9,6a3,3,0,1,0,3,3A3.0033,3.0033,0,0,0,9,6Z" ]
        []
    , path
        [ d "M0.029 15H31.97V17H0.029z", transform "rotate(-45 16 16)" ]
        []
    , path
        [ d "M23,28a5,5,0,1,1,5-5A5.0055,5.0055,0,0,1,23,28Zm0-8a3,3,0,1,0,3,3A3.0033,3.0033,0,0,0,23,20Z" ]
        []
    ]


percentageFilled : List (Svg msg)
percentageFilled =
    [ path
        [ d "M9,14a5,5,0,1,1,5-5A5.0055,5.0055,0,0,1,9,14Z" ]
        []
    , path
        [ d "M0.029 15H31.97V17H0.029z", transform "rotate(-45 16 16)" ]
        []
    , path
        [ d "M23,28a5,5,0,1,1,5-5A5.0055,5.0055,0,0,1,23,28Z" ]
        []
    ]


quotes : List (Svg msg)
quotes =
    [ path
        [ d "M12 15H6.11A9 9 0 0110 8.86l1.79-1.2L10.69 6 8.9 7.2A11 11 0 004 16.35V23a2 2 0 002 2h6a2 2 0 002-2V17A2 2 0 0012 15zM26 15H20.11A9 9 0 0124 8.86l1.79-1.2L24.7 6 22.9 7.2A11 11 0 0018 16.35V23a2 2 0 002 2h6a2 2 0 002-2V17A2 2 0 0026 15z" ]
        []
    ]


reflectHorizontal : List (Svg msg)
reflectHorizontal =
    [ path
        [ d "M19.386,15.2105l9-7A1,1,0,0,1,30,9V23a1,1,0,0,1-1.614.79l-9-7a1,1,0,0,1,0-1.5791Z" ]
        []
    , path
        [ d "M15 2H17V30H15z", transform "rotate(-180 16 16)" ]
        []
    , path
        [ d "M13,16a1.001,1.001,0,0,1-.386.79l-9,7A1,1,0,0,1,2,23V9a1,1,0,0,1,1.614-.79l9,7A1.001,1.001,0,0,1,13,16ZM4,20.9556,10.3711,16,4,11.0444Z" ]
        []
    ]


reflectVertical : List (Svg msg)
reflectVertical =
    [ path
        [ d "M16.79,19.386l7,9A1,1,0,0,1,23,30H9a1,1,0,0,1-.79-1.614l7-9a1,1,0,0,1,1.5791,0Z" ]
        []
    , path
        [ d "M15 2H17V30H15z", transform "rotate(-90 16 16)" ]
        []
    , path
        [ d "M16,13a1.001,1.001,0,0,1-.79-.386l-7-9A1,1,0,0,1,9,2H23a1,1,0,0,1,.79,1.614l-7,9A1.001,1.001,0,0,1,16,13ZM11.0444,4,16,10.3711,20.9556,4Z" ]
        []
    ]


rotateClockwise : List (Svg msg)
rotateClockwise =
    [ path
        [ d "M28 30H16a2.0023 2.0023 0 01-2-2V16a2.0023 2.0023 0 012-2H28a2.0023 2.0023 0 012 2V28A2.0023 2.0023 0 0128 30zM16 16V28H28.0012L28 16zM15 2L13.59 3.41 16.17 6H11a7.0078 7.0078 0 00-7 7v5H6V13a5.0057 5.0057 0 015-5h5.17l-2.58 2.59L15 12l5-5z" ]
        []
    ]


rotateClockwiseAlt : List (Svg msg)
rotateClockwiseAlt =
    [ path
        [ d "M16 30H4a2.0023 2.0023 0 01-2-2V16a2.0023 2.0023 0 012-2H16a2.0023 2.0023 0 012 2V28A2.0023 2.0023 0 0116 30zM4 16V28H16.0012L16 16zM30 15l-1.41-1.41L26 16.17V11a7.0078 7.0078 0 00-7-7H14V6h5a5.0057 5.0057 0 015 5v5.17l-2.59-2.58L20 15l5 5z" ]
        []
    ]


rotateClockwiseAltFilled : List (Svg msg)
rotateClockwiseAltFilled =
    [ path
        [ d "M16 30H4a2.0023 2.0023 0 01-2-2V16a2.0023 2.0023 0 012-2H16a2.0023 2.0023 0 012 2V28A2.0023 2.0023 0 0116 30zM30 15l-1.41-1.41L26 16.17V11a7.0078 7.0078 0 00-7-7H14V6h5a5.0057 5.0057 0 015 5v5.17l-2.59-2.58L20 15l5 5z" ]
        []
    ]


rotateClockwiseFilled : List (Svg msg)
rotateClockwiseFilled =
    [ path
        [ d "M28 30H16a2.0023 2.0023 0 01-2-2V16a2.0023 2.0023 0 012-2H28a2.0023 2.0023 0 012 2V28A2.0023 2.0023 0 0128 30zM15 2L13.59 3.41 16.17 6H11a7.0078 7.0078 0 00-7 7v5H6V13a5.0057 5.0057 0 015-5h5.17l-2.58 2.59L15 12l5-5z" ]
        []
    ]


rotateCounterclockwise : List (Svg msg)
rotateCounterclockwise =
    [ path
        [ d "M2 28V16a2.0023 2.0023 0 012-2H16a2.0023 2.0023 0 012 2V28a2.0023 2.0023 0 01-2 2H4A2.0023 2.0023 0 012 28zM4 16L3.9988 28H16V16zM17 2l1.41 1.41L15.83 6H21a7.0078 7.0078 0 017 7v5H26V13a5.0057 5.0057 0 00-5-5H15.83l2.58 2.59L17 12 12 7z" ]
        []
    ]


rotateCounterclockwiseAlt : List (Svg msg)
rotateCounterclockwiseAlt =
    [ path
        [ d "M14 28V16a2.0023 2.0023 0 012-2H28a2.0023 2.0023 0 012 2V28a2.0023 2.0023 0 01-2 2H16A2.0023 2.0023 0 0114 28zm2-12l-.0012 12H28V16zM2 15l1.41-1.41L6 16.17V11a7.0078 7.0078 0 017-7h5V6H13a5.0057 5.0057 0 00-5 5v5.17l2.59-2.58L12 15 7 20z" ]
        []
    ]


rotateCounterclockwiseAltFilled : List (Svg msg)
rotateCounterclockwiseAltFilled =
    [ path
        [ d "M14 28V16a2.0023 2.0023 0 012-2H28a2.0023 2.0023 0 012 2V28a2.0023 2.0023 0 01-2 2H16A2.0023 2.0023 0 0114 28zM2 15l1.41-1.41L6 16.17V11a7.0078 7.0078 0 017-7h5V6H13a5.0057 5.0057 0 00-5 5v5.17l2.59-2.58L12 15 7 20z" ]
        []
    ]


rotateCounterclockwiseFilled : List (Svg msg)
rotateCounterclockwiseFilled =
    [ path
        [ d "M2 28V16a2.0023 2.0023 0 012-2H16a2.0023 2.0023 0 012 2V28a2.0023 2.0023 0 01-2 2H4A2.0023 2.0023 0 012 28zM17 2l1.41 1.41L15.83 6H21a7.0078 7.0078 0 017 7v5H26V13a5.0057 5.0057 0 00-5-5H15.83l2.58 2.59L17 12 12 7z" ]
        []
    ]


save : List (Svg msg)
save =
    [ path
        [ d "M27.71,9.29l-5-5A1,1,0,0,0,22,4H6A2,2,0,0,0,4,6V26a2,2,0,0,0,2,2H26a2,2,0,0,0,2-2V10A1,1,0,0,0,27.71,9.29ZM12,6h8v4H12Zm8,20H12V18h8Zm2,0V18a2,2,0,0,0-2-2H12a2,2,0,0,0-2,2v8H6V6h4v4a2,2,0,0,0,2,2h8a2,2,0,0,0,2-2V6.41l4,4V26Z" ]
        []
    ]


selectOne : List (Svg msg)
selectOne =
    [ path
        [ d "M12 6L8 6 8 2 6 2 6 6 2 6 2 8 6 8 6 12 8 12 8 8 12 8 12 6zM16 6H20V8H16zM24 6V8h4v4h2V8a2 2 0 00-2-2zM6 16H8V20H6zM8 28V24H6v4a2 2 0 002 2h4V28zM28 16H30V20H28zM16 28H20V30H16zM28 24v4H24v2h4a2 2 0 002-2V24z" ]
        []
    ]


selectTwo : List (Svg msg)
selectTwo =
    [ path
        [ d "M12 6L8 6 8 2 6 2 6 6 2 6 2 8 6 8 6 12 8 12 8 8 12 8 12 6zM30 10V4H24V6H16V8h8v2h2V24H24v2H10V24H8V16H6v8H4v6h6V28H24v2h6V24H28V10zM8 28H6V26H8zm20 0H26V26h2zM26 6h2V8H26z" ]
        []
    ]


spellCheck : List (Svg msg)
spellCheck =
    [ path
        [ d "M20,22h2L17,10H15L10,22h2l1.24-3h5.53Zm-5.93-5,1.82-4.42h.25L18,17Z" ]
        []
    , path
        [ d "M12 28H6a2 2 0 01-2-2V6A2 2 0 016 4H26a2 2 0 012 2V17H26V6H6V26h6zM23 27.18L20.41 24.59 19 26 23 30 30 23 28.59 21.59 23 27.18z" ]
        []
    ]


sprayPaint : List (Svg msg)
sprayPaint =
    [ path
        [ d "M22.5,23A4.5,4.5,0,1,1,27,18.5,4.505,4.505,0,0,1,22.5,23Zm0-7A2.5,2.5,0,1,0,25,18.5,2.5026,2.5026,0,0,0,22.5,16Z" ]
        []
    , path
        [ d "M28 8H26V3H19V8H17a2.002 2.002 0 00-2 2V28a2.0023 2.0023 0 002 2H28a2.0027 2.0027 0 002-2V10A2.0023 2.0023 0 0028 8zM21 5h3V8H21zM17 28V10H28l.002 18zM2 14H5V17H2zM7 9H10V12H7zM2 9H5V12H2zM12 4H15V7H12zM7 4H10V7H7zM2 4H5V7H2z" ]
        []
    ]


textAlignCenter : List (Svg msg)
textAlignCenter =
    [ path
        [ d "M6 6H26V8H6zM10 12H22V14H10zM6 18H26V20H6zM10 24H22V26H10z" ]
        []
    ]


textAlignJustify : List (Svg msg)
textAlignJustify =
    [ path
        [ d "M6 6H26V8H6zM6 12H26V14H6zM6 18H26V20H6zM6 24H26V26H6z" ]
        []
    ]


textAlignLeft : List (Svg msg)
textAlignLeft =
    [ path
        [ d "M12 6H26V8H12zM12 12H22V14H12zM12 18H26V20H12zM12 24H22V26H12zM6 4H8V28H6z" ]
        []
    ]


textAlignRight : List (Svg msg)
textAlignRight =
    [ path
        [ d "M6 6H20V8H6zM10 12H20V14H10zM6 18H20V20H6zM10 24H20V26H10zM24 4H26V28H24z" ]
        []
    ]


textAllCaps : List (Svg msg)
textAllCaps =
    [ path
        [ d "M1 8L1 10 7 10 7 24 9 24 9 10 15 10 15 8 1 8zM17 8L17 10 23 10 23 24 25 24 25 10 31 10 31 8 17 8z" ]
        []
    ]


textBold : List (Svg msg)
textBold =
    [ path
        [ d "M18.25,25H9V7h8.5a5.25,5.25,0,0,1,4,8.65A5.25,5.25,0,0,1,18.25,25ZM12,22h6.23a2.25,2.25,0,1,0,0-4.5H12Zm0-7.5h5.5a2.25,2.25,0,1,0,0-4.5H12Z" ]
        []
    ]


textColor : List (Svg msg)
textColor =
    [ path
        [ d "M22 21h2L17 4H15L8 21h2l1.61-4h8.74zm-9.57-6l3.44-8.37h.26L19.54 15zM6 24H26V28H6z" ]
        []
    ]


textCreation : List (Svg msg)
textCreation =
    [ path
        [ d "M27,22.14V9.86A4,4,0,1,0,22.14,5H9.86A4,4,0,1,0,5,9.86V22.14A4,4,0,1,0,9.86,27H22.14A4,4,0,1,0,27,22.14ZM26,4a2,2,0,1,1-2,2A2,2,0,0,1,26,4ZM4,6A2,2,0,1,1,6,8,2,2,0,0,1,4,6ZM6,28a2,2,0,1,1,2-2A2,2,0,0,1,6,28Zm16.14-3H9.86A4,4,0,0,0,7,22.14V9.86A4,4,0,0,0,9.86,7H22.14A4,4,0,0,0,25,9.86V22.14A4,4,0,0,0,22.14,25ZM26,28a2,2,0,1,1,2-2A2,2,0,0,1,26,28Z" ]
        []
    , path
        [ d "M21 11L11 11 11 13 15 13 15 22 17 22 17 13 21 13 21 11z" ]
        []
    ]


textFill : List (Svg msg)
textFill =
    [ path
        [ d "M4 26H28V30H4z" ]
        []
    , path
        [ d "M26,14.54a1,1,0,0,0-.25-.69L17.17,4.33A1.09,1.09,0,0,0,17,4.2V2H15V5L4.32,14.74a1,1,0,0,0-.06,1.41l8.57,9.52a1,1,0,0,0,.69.33h.05a1,1,0,0,0,.68-.26L24,16.8V21a1,1,0,0,0,2,0V14.57S26,14.55,26,14.54Zm-12.35,9-7.23-8L15,7.67V12h2V7.13l6.59,7.33Z" ]
        []
    ]


textHighlight : List (Svg msg)
textHighlight =
    [ path
        [ d "M12 15H5a3 3 0 01-3-3V10A3 3 0 015 7h5V5A1 1 0 009 4H3V2H9a3 3 0 013 3zM5 9a1 1 0 00-1 1v2a1 1 0 001 1h5V9zM20 23v2a1 1 0 001 1h5V22H21A1 1 0 0020 23z" ]
        []
    , path
        [ d "M2,30H30V2Zm26-2H21a3,3,0,0,1-3-3V23a3,3,0,0,1,3-3h5V18a1,1,0,0,0-1-1H19V15h6a3,3,0,0,1,3,3Z" ]
        []
    ]


textIndent : List (Svg msg)
textIndent =
    [ path
        [ d "M14 6H28V8H14zM14 12H28V14H14zM7 18H28V20H7zM7 24H28V26H7zM4 13.59L7.29 10 4 6.41 5.42 5 10.04 10 5.42 15 4 13.59z" ]
        []
    ]


textIndentLess : List (Svg msg)
textIndentLess =
    [ path
        [ d "M2 6H12V8H2zM5 12H12V14H5zM2 18H12V20H2zM5 24H12V26H5zM16 4H18V28H16zM28.15 23.5L29.56 22.12 23.27 16 29.56 9.88 28.15 8.5 20.44 16 28.15 23.5z" ]
        []
    ]


textIndentMore : List (Svg msg)
textIndentMore =
    [ path
        [ d "M20 6H30V8H20zM20 12H27V14H20zM20 18H30V20H20zM20 24H27V26H20zM14 4H16V28H14zM3.85 22.5L2.44 21.12 8.73 15 2.44 8.88 3.85 7.5 11.56 15 3.85 22.5z" ]
        []
    ]


textItalic : List (Svg msg)
textItalic =
    [ path
        [ d "M25 9L25 7 12 7 12 9 17.14 9 12.77 23 7 23 7 25 20 25 20 23 14.86 23 19.23 9 25 9z" ]
        []
    ]


textKerning : List (Svg msg)
textKerning =
    [ path
        [ d "M30 24L18.83 24 21.12 21.71 19.71 20.29 15 25 19.71 29.71 21.12 28.29 18.83 26 30 26 30 24zM14 21L20 4 18 4 12 21 14 21zM13 4L9 16 5 4 3 4 8 18 10 18 15 4 13 4zM28 18h2L25 4H23L18 18h2l1-3h6zm-6.33-5L24 6l2.33 7z" ]
        []
    ]


textLeading : List (Svg msg)
textLeading =
    [ path
        [ d "M14 13H30V15H14zM15 28H30V30H15zM25.85 27H28L23.64 17H21.36L17 27h2.15l.8-2h5.1zm-5.1-4l1.75-4.37L24.25 23zM25.85 12H28L23.64 2H21.36L17 12h2.15l.8-2h5.1zm-5.1-4L22.5 3.63 24.25 8zM6 15.83L8.58 18.41 10 17 6 13 2 17 3.41 18.42 6 15.83zM6 27.17L3.42 24.59 2 26 6 30 10 26 8.59 24.58 6 27.17z" ]
        []
    ]


textLineSpacing : List (Svg msg)
textLineSpacing =
    [ path
        [ d "M17 6H30V8H17zM17 12H27V14H17zM17 18H30V20H17zM17 24H27V26H17zM11.59 13.41L8 9.83 8 9.83 4.41 13.42 3 12 8 7 13 12 11.59 13.41zM11.59 18.59L8 22.17 8 22.17 4.41 18.58 3 20 8 25 13 20 11.59 18.59z" ]
        []
    ]


textNewLine : List (Svg msg)
textNewLine =
    [ path
        [ d "M20.5859,14.4141,24.1719,18H6V8H4V18a2.0024,2.0024,0,0,0,2,2H24.1719L20.586,23.5859,22,25l6-6-6-6Z" ]
        []
    ]


textScale : List (Svg msg)
textScale =
    [ path
        [ d "M30 5L30 8 22 8 22 26 19 26 19 8 11 8 11 5 30 5z" ]
        []
    , path
        [ d "M7 26L7 14 2 14 2 12 14 12 14 14 9 14 9 26 7 26z" ]
        []
    ]


textSelection : List (Svg msg)
textSelection =
    [ path
        [ d "M28 27H21a3 3 0 01-3-3V22a3 3 0 013-3h5V17a1 1 0 00-1-1H19V14h6a3 3 0 013 3zm-7-6a1 1 0 00-1 1v2a1 1 0 001 1h5V21zM13 7h3V5H13a4 4 0 00-3 1.38A4 4 0 007 5H4V7H7A2 2 0 019 9v5H5v2H9v7a2 2 0 01-2 2H4v2H7a4 4 0 003-1.38A4 4 0 0013 27h3V25H13a2 2 0 01-2-2V16h4V14H11V9A2 2 0 0113 7z" ]
        []
    ]


textSmallCaps : List (Svg msg)
textSmallCaps =
    [ path
        [ d "M23 27L23 15 18 15 18 13 30 13 30 15 25 15 25 27 23 27z" ]
        []
    , path
        [ d "M11 27L11 8 2 8 2 6 22 6 22 8 13 8 13 27 11 27z" ]
        []
    ]


textStrikethrough : List (Svg msg)
textStrikethrough =
    [ path
        [ d "M28,15H17.9563c-.4522-.1237-.9037-.2324-1.3381-.3352-2.8077-.6641-4.396-1.1506-4.396-3.4231a2.8684,2.8684,0,0,1,.7866-2.145,4.7888,4.7888,0,0,1,3.0137-1.09c2.8291-.07,4.1347.8894,5.2011,2.35l1.6153-1.1792a7.4727,7.4727,0,0,0-6.83-3.1706,6.7726,6.7726,0,0,0-4.4,1.6611,4.8274,4.8274,0,0,0-1.3862,3.5735A4.3723,4.3723,0,0,0,11.9573,15H4v2H17.6519c1.9668.57,3.1432,1.3123,3.1733,3.3579a3.119,3.119,0,0,1-.8623,2.3931A5.8241,5.8241,0,0,1,16.2432,24a6.6344,6.6344,0,0,1-5.1451-2.6912L9.5649,22.593A8.5262,8.5262,0,0,0,16.2119,26c.0088-.0012.042,0,.1,0A7.67,7.67,0,0,0,21.36,24.1812a5.0779,5.0779,0,0,0,1.4648-3.8531A4.952,4.952,0,0,0,21.6753,17H28Z" ]
        []
    ]


textSubscript : List (Svg msg)
textSubscript =
    [ path
        [ d "M26 25L20 25 20 19 24 19 24 17 20 17 20 15 26 15 26 21 22 21 22 23 26 23 26 25zM5 7L5 9 12 9 12 25 14 25 14 9 21 9 21 7 5 7z" ]
        []
    ]


textSuperscript : List (Svg msg)
textSuperscript =
    [ path
        [ d "M29 17L23 17 23 11 27 11 27 9 23 9 23 7 29 7 29 13 25 13 25 15 29 15 29 17zM4 7L4 9 11 9 11 25 13 25 13 9 20 9 20 7 4 7z" ]
        []
    ]


textTracking : List (Svg msg)
textTracking =
    [ path
        [ d "M25.29 19.29L23.88 20.71 26.17 23 5.83 23 8.12 20.71 6.71 19.29 2 24 6.71 28.71 8.12 27.29 5.83 25 26.17 25 23.88 27.29 25.29 28.71 30 24 25.29 19.29zM26 17h2L23 3H21L16 17h2l1-3h6zm-6.33-5L22 5l2.33 7zM14 3L10 15 6 3 4 3 9 17 11 17 16 3 14 3z" ]
        []
    ]


textUnderline : List (Svg msg)
textUnderline =
    [ path
        [ d "M4 26H28V28H4zM16 23a7 7 0 01-7-7V5h2V16a5 5 0 0010 0V5h2V16A7 7 0 0116 23z" ]
        []
    ]


textVerticalAlignment : List (Svg msg)
textVerticalAlignment =
    [ path
        [ d "M16 28H30V30H16zM16 23H30V25H16zM10.8458 30H13L8.64 20H6.36L2 30H4.1542l.8-2h5.0916zM5.7541 26L7.5 21.6347 9.2459 26zM2 15H30V17H2zM16 7H30V9H16zM16 2H30V4H16zM10.8458 12H13L8.64 2H6.36L2 12H4.1542l.8-2h5.0916zM5.7541 8L7.5 3.6347 9.2459 8z" ]
        []
    ]


textWrap : List (Svg msg)
textWrap =
    [ path
        [ d "M4 23H12V25H4zM24.5232 14H4v2H24.5a3.5 3.5 0 010 7H18.8281l2.586-2.5859L20 19l-5 5 5 5 1.4141-1.4141L18.8281 25H24.533a5.5 5.5 0 00-.01-11zM4 5H28V7H4z" ]
        []
    ]


trashCan : List (Svg msg)
trashCan =
    [ path
        [ d "M12 12H14V24H12zM18 12H20V24H18z" ]
        []
    , path
        [ d "M4 6V8H6V28a2 2 0 002 2H24a2 2 0 002-2V8h2V6zM8 28V8H24V28zM12 2H20V4H12z" ]
        []
    ]


unlink : List (Svg msg)
unlink =
    [ path
        [ d "M5 3.59H7V8.42H5z", transform "rotate(-45.01 5.996 6.005)" ]
        []
    , path
        [ d "M25 23.58H27V28.409999999999997H25z", transform "rotate(-44.99 25.995 25.999)" ]
        []
    , path
        [ d "M11 2H13V6H11zM2 11H6V13H2zM26 19H30V21H26zM19 26H21V30H19zM16.58 21.07l-3.71 3.72a4 4 0 11-5.66-5.66l3.72-3.72L9.51 14 5.8 17.72a6 6 0 00-.06 8.54A6 6 0 0010 28a6.07 6.07 0 004.32-1.8L18 22.49zM15.41 10.93l3.72-3.72a4 4 0 115.66 5.66l-3.72 3.72L22.49 18l3.71-3.72a6 6 0 00.06-8.54A6 6 0 0022 4a6.07 6.07 0 00-4.32 1.8L14 9.51z" ]
        []
    ]
