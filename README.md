# elm-carbon

Implementation des [Carbon Design System von IBM](https://www.ibm.com/design/language/).

## Voraussetzungen
- Es muss die Version 0.19.x von [Elm](https://elm-lang.org) installiert sein.
- Ebenso die entsprechende Version von [Elm-Test](https://www.npmjs.com/package/elm-test).
- Zur Erzeugung der API-Dokumentation wird das Tool [elm-doc-preview](https://github.com/dmy/elm-doc-preview/) verwendet.

## Verzeichnisse
- `features` - Verzeichnis für die automatisierten Akzeptanz-/End-2-End-Tests. wird vom Tool namens [Cucumber](https://cucumber.io) verwendet.
  Die Realisierung der Step-Definitionen werden zusammen mit dem Tool [Puppeteer](https://pptr.dev) vorgenommen, um das UI zu testen.
  Einen guten Überblick hierzu gibt es unter https://medium.com/@anephenix/end-to-end-testing-single-page-apps-and-node-js-apis-with-cucumber-js-and-puppeteer-ad5a519ace0
  (Letzter Abruf: 08.10.2020, 10:15 Uhr).
- `public` - Build-Verzeichnis mit der resultierenden 
  `index.html` sowie den Assets-Ordnern `css`, `js` und
  `img` für die verwendeten Stylesheets, JS-Skripte und
  Bilddateien respektiv. Dieser Ordner wird später auf dem Produktiv-Server verwendet.
- `src`: Quellcode des Projektes, geschrieben in Elm.
- `tests`: Verzeichnis mit Unit-Tests zum Projekt. Verwendet
  Elm-Test

## Benutzung

Zum Installieren der lokalen Entwicklungstools (Cucumber etc.):

```
> npm i
```

Zum Ausführen der Unit-Test auf dem Terminal / der Konsole:

```
> elm-test
```

Zum Ausführen der UI- bzw. End-2-End-Tests:

```
> npm test
```

Zum Bauen des Projektes:

```
> npm run make
```

Das gebaute Projekt kann dann über einem Webserver wie z. B. [http-server](https://www.npmjs.com/package/http-server) oder `elm reactor` ( und laden der `index.html`) lokal angesehen werden (innerhalb von `public/`). 