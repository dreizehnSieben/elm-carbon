const { setWorldConstructor } = require('@cucumber/cucumber');
const puppeteer = require('puppeteer');
const assert = require('assert').strict;

const PAGE = `${process.env.CEEQUINO_BASE}`;

class CeequinoWorld {
    async openBrowser() {
        this.browser = await puppeteer.launch({headless: true});
        this.page = await this.browser.newPage();
    }

    async gotoCeequino() {
        await this.page.goto(PAGE);
        let result = await this.page.$('nav');
        assert.ok(result);
    }

    async gotoPerson() {
        await this.page.goto(`${PAGE}/person`);
    }

    async closeBrowser() {
        await this.browser.close();
    }
}

setWorldConstructor(CeequinoWorld);