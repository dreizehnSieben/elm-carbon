module UI.Components.Accordion exposing (..)

import Html exposing (Html)

type alias AccordionItem msg =
    { title: String
    , content: Html msg
    }

type Alignment
    = Start
    | End

type alias Accordion msg =
    { alignment: Alignment
    , items: List (AccordionItem msg)
    }
