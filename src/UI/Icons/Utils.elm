module UI.Icons.Utils exposing (icon16, icon20, icon24, icon32)

import Element exposing (Element, html)
import Svg exposing (Svg, svg)
import Svg.Attributes exposing (height, viewBox, width, x, y)


icon32 : List (Svg msg) -> Element msg
icon32 =
    icon 32


icon24 : List (Svg msg) -> Element msg
icon24 =
    icon 24


icon20 : List (Svg msg) -> Element msg
icon20 =
    icon 20


icon16 : List (Svg msg) -> Element msg
icon16 =
    icon 16


icon : Int -> List (Svg msg) -> Element msg
icon size data =
    html <|
        svg
            [ x "0"
            , y "0"
            , width <| String.fromInt size
            , height <| String.fromInt size
            , viewBox "0 0 32 32"
            ]
            data
