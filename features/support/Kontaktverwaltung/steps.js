const { After, Before, Given, When, Then } = require('@cucumber/cucumber');

Before(async function () {
    await this.openBrowser();
});

Given('der Benutzer befindet sich in der Anwendung', async function () {
    await this.gotoCeequino();
});

Given('der Benutzer sieht das Formular zum Anlegen einer Person', async function () {
    await this.gotoPerson();
    await this.openNewPersonForm();
});

When('die Pflichtfelder ausgefüllt wurden', function () {
    await this.fillInRequiredPersonData();
});

When('die Eingaben vom Benutzer gespeichert werden', function () {
    await this.savePerson();
});

Then('sieht der Benutzer diese Person in der Kontaktübersicht', function () {
    await this.checkPersonCount();
});


After(async function () {
    await this.closeBrowser();
});